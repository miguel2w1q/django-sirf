#!/bin/sh

# Create super user
echo "from apps.users.models import User; User.objects.create_superuser('admin','','admin')" | python3 manage.py shell || true

# Create groups
echo "from django.contrib.auth.models import Group; Group.objects.create(name='coordinator'); Group.objects.create(name='driver'); Group.objects.create(name='passenger')" | python3 manage.py shell || true

# Load default rate values
python manage.py loaddata apps/ride/fixtures/rate.json

# Grupo America Sirf Django App

Build with: 
- python 3.7.7
- psql 12.2
- redis-server 5.0.7
- django 3.0.5

## Local Development

### Requirements

- python 3.7.7
- pip 19.2.3
- psql 12.2
- redis-server 5.0.7

To install django and this app dependencies run:
```
pip install -r requeriments/local.txt
```

### Enviroment Variables
Set this variable to work locally
```
export DJANGO_SETTINGS_MODULE=config.settings.local
```
In this path you will put your enviroment variables
> /.envs/.local/.django
> /.envs/.local/.postgres

### Inital data
Load initial data with this script:
```
. super.sh
```

### Launch App
Be sure that the redis-server and db works
>App
```
python manage.py runserver 0.0.0.0:8000
```
>Worker
```
python manage.py rqworker high default low
```
>Scheduler
```
rqscheduler
```

## Production

### AWS Requirements
- ElasticBean
- RDS
- ElastiCache
- S3
- Route53
- IAM
- CodeCommit
- CertificateManager
- EC2
- VPC

### Enviroment Variables
In this path you will put your enviroment variables
> /.envs/.production/.django
> /.envs/.production/.postgres

Remember to run this script if you change any enviroment variable and verify that you did not change the `PRODUCTION_DOTENVS_DIR_PATH`
```
python merge_production_dotenvs_in_dotenv.py
```


### Certifcate Manager
Request a certificate with "your.domain"


### IAM
Create a Group and attach full access to next services:
- AmazonRDSFullAccess
- AmazonEC2FullAccess
- AmazonS3FullAccess
- AWSElasticBeanstalkFullAccess
- AmazonVPCFullAccess
- AmazonRoute53FullAccess
Then create a User and create his `Access Key ID` and `Secret Access ID` that you will need to use `awsebcli`


### VPC Security Groups
In Vpc you can add a security Group or use the default created by EB, but add the next inbound rules:
![sc](docs/vpc_sg.png)

### Route53
Create a HostedZone in Route53 with public domain and "your.domain", change the dns of your domain provider to value that the NS Record provides:
![r53](docs/route53_records.png)
in our case its:
```
ns-81.awsdns-10.com.
ns-1312.awsdns-36.org.
ns-549.awsdns-04.net.
ns-1897.awsdns-45.co.uk.
```
after that you'll need to create an `A Record` without prefix an add your endpoint provided by your eb instance and `CNAME Record` with the same value but with `www` prefix.

### S3
Create a bucket to serve our application static files, don't block his public access and in permission add a Bucket Policy:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion"
            ],
            "Resource": "arn:aws:s3:::your-bucket-name/*",
            "Condition": {
                "StringLike": {
                    "aws:Referer": [
                        "https://your.domain/*",
                        "htttps://www.your.domain/*"
                    ]
                }
            }
        }
    ]
}
```

### RDS
You need to create a postgres db instance and set its pubic access to "Yes", we don't ned the Multi-AZ deployment for now and choose a security group that has a inboud rule for everyhost in the specified port.
![rds](docs/rds_connectivity.png)

### ElastiCache
Create a Redis cluster, again we don't neet Multi-AZ, but specify the security that has our specified ports open.
![rds](docs/ec_security.png)

### Deploy with eb
- Just follow the [instructions](https://github.com/aws/aws-elastic-beanstalk-cli-setup) to install awsebcli.
- Start with `eb init`
- Specify your aws credentials
- server region (us-east-1)
- platform(Python 3.7 running on 64bit Amazon Linux 2)
- Classic Load Balancer
- `eb create production` to create an enviroment named "production"
- Commit your changes before deploy changes
- `eb deploy` to upload new code

from drf_yasg import openapi
from rest_framework import serializers

from .utils import ts_to_datetime


class TimeStampField(serializers.Field):
    class Meta:
        swagger_schema_fields = {
            'type': openapi.TYPE_INTEGER,
        }

    def to_representation(self, value):
        return int(value.strftime('%s'))

    def to_internal_value(self, data):
        return ts_to_datetime(data)

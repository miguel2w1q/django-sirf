from django.contrib.auth.mixins import LoginRequiredMixin
from drf_yasg.utils import swagger_auto_schema
from rest_framework.pagination import PageNumberPagination


class NoPatchView:

    @swagger_auto_schema(auto_schema=None)
    def patch(self):
        pass


class EstandarPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

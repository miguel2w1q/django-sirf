import datetime

from apps.common.utils import ts_to_datetime
from django.forms.fields import BaseTemporalField
from django.forms.widgets import DateInput
from django.utils import formats
from django.utils.translation import gettext_lazy as _
from django_filters.filters import Filter


class TimeStampDateField(BaseTemporalField):
    widget = DateInput
    input_formats = formats.get_format_lazy('DATE_INPUT_FORMATS')
    default_error_messages = {
        'invalid': _('Enter a valid date.'),
    }

    def to_python(self, value):
        """
        Validate that the input can be converted to a date. Return a Python
        datetime.date object.
        """
        if value in self.empty_values:
            return None
        if isinstance(value, datetime.datetime):
            return value.date()
        if isinstance(value, datetime.date):
            return value
        return super().to_python(value)

    def strptime(self, value, format):
        return ts_to_datetime(value)


class TimeStampDateFilter(Filter):
    field_class = TimeStampDateField

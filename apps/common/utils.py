from datetime import datetime
from io import BytesIO

from django.core.mail import EmailMessage
from django.conf import settings
from pytz import timezone

from openpyxl import Workbook
from openpyxl.styles import Font, PatternFill
from openpyxl.utils import get_column_letter

header_color = "900C3F"
row_color_1 = "DAF7A6"
row_color_2 = "FFF9D8"
white_color = "FFFFFF"
black_color = "000000"


def ts_to_datetime(ts):
    return datetime.fromtimestamp(int(ts), timezone(settings.TIME_ZONE))

def add_tz(dt):
    return timezone(settings.TIME_ZONE).localize(dt)


class ExcelFile:
    wb = None
    ws = None
    extra_emails = [settings.EMAIL_HOST_USER]
    name = None

    def __init__(self, name="Programación"):
        self.wb = Workbook()
        self.ws = self.wb.active
        self.name = name

    def cell(self, col, row,  value, color, colorTxt):
        c = self.ws.cell(row=row, column=col)
        c.value = value
        c.fill = PatternFill(fgColor=color, fill_type="solid")
        c.font = Font(color=colorTxt)

    def row(self, col_start, row_start, arr, color, colorTxt):
        for i, val in enumerate(arr):
            self.cell(i+col_start, row_start, val, color, colorTxt)

    def add_columns_width(self, start, widths):
        for i, val in enumerate(widths):
            self.ws.column_dimensions[get_column_letter(i+start+1)].width = val

    def add_email(self, email):
        self.extra_emails.append(email)

    def add_headers(self, col_start, row_start, data):
        self.add_columns_width(col_start, [d['width'] for d in data])
        self.row(col_start, row_start, [d['text'] for d in data], header_color, white_color)

    def change_ws(self, name):
        if 'Sheet' in self.wb.sheetnames:
            self.ws.title = name
        if name in self.wb.sheetnames:
            self.ws = self.wb[name]
        else:
            self.ws = self.wb.create_sheet(name)

    def send(self):
        excelfile = BytesIO()
        self.wb.save(excelfile)
        email = EmailMessage()
        time = datetime.datetime.now().strftime("%d/%m/%Y, %H:%M")
        email.subject = f"{self.name} {time}"
        email.body = "Revisar el adjunto."
        email.from_email = settings.EMAIL_HOST_USER
        email.to = self.extra_emails
        email.attach(f"{email.subject}.xlsx", excelfile.getvalue(), 'application/ms-excel')
        email.send()

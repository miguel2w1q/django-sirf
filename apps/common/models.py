from datetime import datetime

from apps.users.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models


class AuditableModel(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        abstract = True

    def modified_by(self, user):
        if not self.created_by:
            self.created_by = user
        self.modified_by = user


class TimeStampModel(models.Model):
    created = models.DateTimeField('Creation date', auto_now_add=True, editable=False)
    modified = models.DateTimeField('Modification date', auto_now=True, editable=False)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.modified = datetime.now()
        super().save(*args, **kwargs)


class PersonModel(AuditableModel, TimeStampModel):
    user = models.OneToOneField(User, on_delete=models.PROTECT, blank=True, null=True)
    phones = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    first_name = models.CharField(max_length=15, null=True, blank=True)
    last_name = models.CharField(max_length=15, null=True, blank=True)

    class Meta:
        abstract = True

from django.db import models


class Pais(models.Model):
    name = models.CharField('Name', max_length=127)

    def __str__(self):
        return self.name


class Departamento(models.Model):
    country = models.ForeignKey(Pais, on_delete=models.PROTECT)
    name = models.CharField('Name', max_length=127)

    def __str__(self):
        return self.name


class Provincia(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.PROTECT)
    name = models.CharField('Name', max_length=127)

    def __str__(self):
        return self.name


class Distrito(models.Model):
    postal_code = models.CharField('Postal code', max_length=5, unique=True)
    provincia = models.ForeignKey(Provincia, on_delete=models.PROTECT, null=True)
    name = models.CharField('Name', max_length=127)

    def __str__(self):
        return self.name

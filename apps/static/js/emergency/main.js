class EmergencyManager {
    constructor() {
        this.apis = arguments[0].apis
        this.rowRender = arguments[0].rowRender
    }

    list() {
        let me = this
        ax.get(me.apis.list)
            .then(function (response) {
                let tableBody = document.getElementById("tableBody")
                let rows = ''
                response.data.forEach((element, index) => {
                    let columns = `<tr>`
                    element['index'] = ++index
                    me.rowRender.forEach(f => {
                        columns += `<td>${f(element)}</td>`
                    })
                    columns += '</tr>'
                    rows += columns
                })
                tableBody.innerHTML = rows
                document.querySelectorAll('.combo-emergency').forEach(combo => {
                    addPrevent(combo, me.updateEmergency.bind(me), 'change')
                })
            })
        //.catch(function (error) {
        //    swal.fire({ text: 'No se pudo obtener las emergencias', icon: 'error' })
        //})
    }

    clearEntity() {
        let me = this
        Object.values(me.input).forEach(input => input.value = '')
    }

    updateEmergency(id) {
        let me = this
        let val = document.querySelector(`.combo-emergency[data-id="${id}"]`).value
        let data = {
            'operator_status': val === "null"? null: parseInt(val)
        }
        return ax.put(
            me.apis.update + id, data
        ).then(() => alertOk('actualizado')
        ).catch(() => alertError('No se pudo actualizar'))
    }

    getAction(id) {
        let me = this
        if (me.btnSubmit.getAttribute("action") == "update") return me.update(id)
        else return me.create()

    }

    retrieve(id) {
        let me = this
        me.btnSubmit.setAttribute("action", "update")
        me.btnSubmit.setAttribute("data-id", id)
        me.input.Password.value = ''
        return ax.get(me.apis.retrieve + id)
            .then(function (response) {
                me.setEntity(response.data)
                me.formModal.show()
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
            })
    }

    update() {
        let me = this
        let id = me.btnSubmit.getAttribute('data-id')
        return ax.put(me.apis.update + id, me.getEntity())
            .then(function (response) {
                me.list()
                me.clearEntity()
                me.formModal.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al actualizar la informacion', icon: 'error' })
            })
    }

    create() {
        let me = this
        return ax.post(me.apis.create, me.getEntity())
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
                me.formModal.hide()
                me.clearEntity()
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }

    delete(id) {
        let me = this
        return ax.delete(me.apis.delete + id)
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo eliminar'
                })
            })
    }
}


document.addEventListener("DOMContentLoaded", function () {
    let options = {
        apis: api.emergency,
        rowRender: [
            data => { return `${data.id}` },
            data => { return `${data.driver.first_name} ${data.driver.last_name}` },
            data => { return `${data.passenger.first_name} ${data.passenger.last_name}` },
            data => { return `${DRIFT_STATUS_CHOICES[data.status]}` },
            data => { return `${data.place.distrito}` },
            data => { return createCombo(DRIFT_OPERATOR_STATUS_CHOICES, data.operator_status, data.id, 'combo-emergency') }
        ]
    }
    let emergencyManager = new EmergencyManager(options)
    emergencyManager.list()
});
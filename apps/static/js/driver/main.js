class DriverManager {
    constructor() {
        this.apis = arguments[0].apis
        this.rowRender = arguments[0].rowRender
        this.btnSubmit = document.getElementById("btnSubmit")
        this.formModalElement = document.getElementById("formModal")
        this.formModal = new mdb.Modal(this.formModalElement)
        this.vechicles = []
        this.input = {
            Dni: document.getElementById("inputDNI"),
            FirstName: document.getElementById("inputFirstName"),
            LastName: document.getElementById("inputLastName"),
            Email: document.getElementById("inputEmail"),
            Phones: document.getElementById("inputPhones"),
            CarModel: document.getElementById("inputCarModel"),
            CarBrand: document.getElementById("inputCarBrand"),
            CarPlate: document.getElementById("inputCarPlate"),
            CarCapacity: document.getElementById("inputCarCapacity"),
            Password: document.getElementById("inputPassword"),
        }
        addPrevent(this.btnSubmit, this.getAction.bind(this))
        document.getElementById("createBtnModal").addEventListener('click', () => {
            this.clearEntity()
            this.input.Password.value = Math.floor(Math.random() * (900000)) + 100000;
            this.btnSubmit.removeAttribute('data-id')
            this.btnSubmit.setAttribute('action', 'create')
            this.formModal.show()
        })

        this.loadDataLists()
        this.formModalElement.addEventListener('shown.bs.modal', fixNotch)
    }

    loadDataLists() {
        let me = this
        createDataList('carBrandList', '/api/fleet/brand', 'brand')
        createDataList('carModelList', '/api/fleet/model', 'model')
    }
    list() {
        let me = this
        ax.get(me.apis.list)
            .then(function (response) {
                let tableBody = document.getElementById("tableBody")
                let rows = ''
                response.data.forEach((element, index) => {
                    let columns = `<tr>`
                    element['index'] = ++index
                    me.rowRender.forEach(f => {
                        columns += `<td>${f(element)}</td>`
                    })
                    columns += '</tr>'
                    rows += columns
                })
                tableBody.innerHTML = rows
                document.querySelectorAll("button.update").forEach(button => {
                    addPrevent(button, me.retrieve.bind(me))
                })
                document.querySelectorAll("button.delete").forEach(button => {
                    addPrevent(button, me.delete.bind(me))
                })
            }).catch(function (error) {
                swal.fire({ text: 'No se pudo obtener los conductores', icon: 'error' })
            })
    }

    getEntity() {
        let me = this
        return {
            username: me.input.Dni.value,
            first_name: me.input.FirstName.value,
            last_name: me.input.LastName.value,
            email: me.input.Email.value,
            phones: me.input.Phones.value.split(',').map(e => e.trim()),
            password: me.input.Password.value,
            car: {
                model: me.input.CarModel.value,
                brand: me.input.CarBrand.value,
                plate: me.input.CarPlate.value,
                capacity: me.input.CarCapacity.value,
            }
        }
    }

    setEntity(data) {
        let me = this
        me.input.Dni.value = data.username
        me.input.FirstName.value = data.first_name
        me.input.LastName.value = data.last_name
        me.input.Email.value = data.email
        me.input.Phones.value = data.phones.join(', ')
        me.input.CarModel.value = data.car ? data.car.model : ''
        me.input.CarBrand.value = data.car ? data.car.brand : ''
        me.input.CarPlate.value = data.car ? data.car.plate : ''
        me.input.CarCapacity.value = data.car ? data.car.capacity : ''
        me.input.Password.value = data.plain_password
    }

    clearEntity() {
        let me = this
        Object.values(me.input).forEach(input => input.value = '')
    }

    getAction(id) {
        let me = this
        if (me.btnSubmit.getAttribute("action") == "update") return me.update(id)
        else return me.create()

    }

    retrieve(id) {
        let me = this
        me.btnSubmit.setAttribute("action", "update")
        me.btnSubmit.setAttribute("data-id", id)
        me.input.Password.value = ''
        return ax.get(me.apis.retrieve + id)
            .then(function (response) {
                me.setEntity(response.data)
                me.formModal.show()
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
            })
    }

    update() {
        let me = this
        let id = me.btnSubmit.getAttribute('data-id')
        return ax.put(me.apis.update + id, me.getEntity())
            .then(function (response) {
                me.list()
                me.clearEntity()
                me.formModal.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al actualizar la informacion', icon: 'error' })
            })
    }

    create() {
        let me = this
        return ax.post(me.apis.create, me.getEntity())
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
                me.formModal.hide()
                me.clearEntity()
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }

    delete(id) {
        let me = this
        return ax.delete(me.apis.delete + id)
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo eliminar'
                })
            })
    }
}


document.addEventListener("DOMContentLoaded", function () {
    let options = {
        apis: api.driver,
        rowRender: [
            data => { return data.index },
            data => { return data.username },
            data => { return data.first_name },
            data => { return data.last_name },
            data => {
                if (data.car) return `${data.car.model} ${data.car.plate}`
                else return ''
            },
            data => {
                return `<button type="button" class="btn btn-primary update" data-id=${data.id}>Editar</button>
                              <button type="button" class="btn btn-danger delete" data-id="${data.id}">Eliminar</button>`
            },
        ]
    }
    let driverManager = new DriverManager(options)
    driverManager.list()
});
class Drift {
    constructor(input, api, manager) {
        this.manager = manager
        this.api = api
        this.ride = null
        this.data = null
        this.input = input
        this.formModalElement = document.getElementById("formModalDrift")
        this.formModal = new mdb.Modal(this.formModalElement)
        this.btnSubmit = document.getElementById("btnSubmitDrift")
        addPrevent(this.btnSubmit, this.getAction.bind(this))
        this.formModalElement.addEventListener('shown.bs.modal', fixNotch)
        document.getElementById("btnCreateDrift").addEventListener('click', () => {
            this.btnSubmit.removeAttribute('data-id')
            this.btnSubmit.setAttribute('action', 'create')
            this.clearEntity()
            this.formModal.show()
        })
    }
    getAction(id) {
        let me = this
        if (me.btnSubmit.getAttribute("action") == "update") return me.update(id)
        else return me.create()
    }
    setUpdate(id) {
        let me = this
        me.btnSubmit.setAttribute("action", "update")
        me.btnSubmit.setAttribute("data-id", id)
        return ax.get(me.api + id)
            .then(function (response) {
                me.data = response.data
                me.draw()
                me.formModal.show()
            })
        .catch(function (error) {
            swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
        })

    }
    clearEntity() {
        let me = this
        me.input.passenger_dni.value = ''
        me.input.passenger_first_name.value = ''
        me.input.passenger_last_name.value = ''
        me.input.address_name.value = ''
        me.input.status.value = ''
        me.input.operator_status.value = ''
        me.input.rate_code.setChoiceByValue('')
        me.input.rate.value = ''
    }
    getEntity() {
        let me = this
        let os = me.input.operator_status.value
        return {
            ride: me.ride,
            dni: me.input.passenger_dni.value,
            first_name: me.input.passenger_first_name.value,
            last_name: me.input.passenger_last_name.value,
            address: me.input.address_name.value,
            os: os == "null" ? null : os,
            rate_code: me.input.rate_code.getValue(true),
            rate: me.input.rate.value
        }
    }
    update(id) {
        let me = this
        return ax.put(me.api + id, me.getEntity())
            .then(function () {
                me.clearEntity()
                me.formModal.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            })
        .catch(function () {
            swal.fire({ text: 'Problemas al actualizar la informacion', icon: 'error' })
        })
    }
    create() {
        let me = this
        return ax.post(me.api, me.getEntity())
            .then(function (response) {
                swal.fire({
                    icon: 'success', timer: 600
                })
                me.manager.ride.setUpdate(me.ride)
                me.formModal.hide()
                me.clearEntity()
            })
            .catch(function () {
                alertError('No se pudo crear')
            })
    }

    draw() {
        let me = this
        for (let [key, input] of Object.entries(me.input)) {
            if (input instanceof Choices) input.setChoiceByValue(me.data[key])
            else input.value = me.data[key]
        }
    }
}

class Ride {
    constructor(input, api, drift, manager) {
        this.drift = drift
        this.data = null
        this.input = input
        this.drifts = []
        this.api = api
        this.manager = manager
        this.driftsTable= document.getElementById('driftsTable')
        this.driftsTableBody = document.getElementById('driftsTableBody')
        this.formModalElement = document.getElementById("formModalRide")
        this.formModal = new mdb.Modal(this.formModalElement)
        this.btnSubmit = document.getElementById("btnSubmitRide")
        addPrevent(this.btnSubmit, this.getAction.bind(this))

        this.formModalElement.addEventListener('shown.bs.modal', fixNotch)
        flatpickr(this.input.scheduled_time, {
            enableTime: true,
            dateFormat: "d-m-Y H:i",
        })
    }
    getAction(id) {
        let me = this
        if (me.btnSubmit.getAttribute("action") == "update") return me.update(id)
        else return me.create()
    }
    setUpdate(id) {
        let me = this
        me.btnSubmit.setAttribute("action", "update")
        me.btnSubmit.setAttribute("data-id", id)
        me.driftsTable.classList.remove('hide')
        me.drift.ride = id 
        return ax.get(me.api.retrieve + id)
            .then(function (response) {
                me.data = response.data
                me.draw()
                me.formModal.show()
            })
        .catch(function (error) {
            swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
        })

    }
    setCreate() {
        let me = this
        me.btnSubmit.setAttribute("action", "create")
        me.btnSubmit.setAttribute("data-id", '')
        me.driftsTable.classList.add('hide')
        me.clearEntity()
        me.formModal.show()
    }
    draw() {
        let me = this
        for (let [key, input] of Object.entries(me.input)) {
            if (input instanceof Choices) input.setChoiceByValue(me.data[key])
            else input.value = me.data[key]
        }
        me.driftsTableBody.innerHTML = ''
        me.list(me.data.drifts)
    }
    list(data) {
        let me = this
        let tableBody = me.driftsTableBody
        let rows = ''
        let rowRender = [
            data => { return `DC-${data.id}` },
            data => { return data.passenger },
            data => { return DRIFT_STATUS_CHOICES[parseInt(data.status)] },
            data => { return data.rate_code },
            data => {
                return `<button type="button" class="btn btn-primary drift-update" data-id=${data.id}>Editar</button>
                        <button type="button" class="btn btn-danger drift-delete" data-id=${data.id}>Eliminar</button>`
            },
        ]
        data.forEach((element, index) => {
            let columns = `<tr>`
            element['index'] = ++index
            rowRender.forEach(f => {
                columns += `<td>${f(element)}</td>`
            })
            columns += '</tr>'
            rows += columns
        })
        tableBody.innerHTML = rows
        document.querySelectorAll('.drift-update').forEach(button => {
            addPrevent(button, me.retrieve.bind(me))
        })
        document.querySelectorAll('.drift-delete').forEach(button => {
            addPrevent(button, me.delete.bind(me))
        })
    }
    retrieve(id) {
        let me = this
        return me.drift.setUpdate(id)
    }
    delete(id) {
        let me = this
        return alertDelete('¿Estás seguro de eliminar?').then(result => {
            if (result.value) ax.delete(me.api.drift + id)
                .then(function () {
                    document.querySelector(`.drift-delete[data-id="${id}"]`).closest("tr").remove()
                    swal.fire({
                        icon: 'success',
                        timer: 600
                    })
                }).catch(function () {
                    swal.fire({
                        icon: 'error',
                        timer: 600,
                        text: 'No se pudo eliminar'
                    })
                })
        })
    }
    clearEntity() {
        let me = this
        me.input.driver.setChoiceByValue("")
        me.input.company.setChoiceByValue("")
        me.input.car.setChoiceByValue("")
        me.input.scheduled_time.value = ''
        me.input.address_name.value = ''
    }
    getEntity() {
        let me = this
        return {
            driver: me.input.driver.getValue(true),
            company: me.input.company.getValue(true),
            car: me.input.car.getValue(true),
            scheduled_time: me.input.scheduled_time.value,
            address: me.input.address_name.value
        }
    }
    update(id) {
        let me = this
        return ax.put(me.api.update + id, me.getEntity())
            .then(function () {
                me.clearEntity()
                me.formModal.hide()
                me.manager.list()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }) .catch(function () {
            swal.fire({ text: 'Problemas al actualizar la informacion', icon: 'error' })
        })
    }
    create() {
        let me = this
        return ax.post(me.api.create, me.getEntity())
            .then(function (response) {
                me.manager.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
                me.formModal.hide()
                me.clearEntity()
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }
}


class RideManager {
    constructor() {
        this.api = arguments[0].api
        this.rowRender = arguments[0].rowRender
        this.inputDrift = {
            passenger_dni: document.getElementById('inputDriftDni'),
            passenger_first_name: document.getElementById('inputDriftFirstName'),
            passenger_last_name: document.getElementById('inputDriftLastName'),
            address_name: document.getElementById('inputDriftAddress'),
            status: document.getElementById('inputDriftStatus'),
            operator_status: document.getElementById('inputDriftOperatorStatus'),
            rate_code: simpleChoice('inputDriftRate'),
            rate: document.getElementById('inputDriftPrice')
        }
        this.drift = new Drift(this.inputDrift, this.api.drift, this)
        this.inputRide = {
            driver: simpleChoice('inputDriver'),
            company: simpleChoice('inputCompany'),
            scheduled_time: document.getElementById('inputScheduledTime'),
            address_name: document.getElementById('inputAddress'),
            car: simpleChoice('inputVehicle')
        }
        this.ride = new Ride(this.inputRide, this.api, this.drift, this)
        this.page = 1
        this.page_size = 10
        this.inputFilters = {
            scheduled_begin: document.getElementById("inputFilterScheduledTimeBegin"),
            scheduled_end: document.getElementById("inputFilterScheduledTimeEnd"),
            company: simpleChoice('inputFilterCompany'),
            driver: simpleChoice('inputFilterDriver'),
        }
        this.companies = null
        this.drivers = null

        this.btnSearch = document.getElementById('btnSearch')
        this.btnOpenCreateRide = document.getElementById('btnOpenCreateRide')
        this.btnOpenCreateRide.addEventListener('click', ()=>{
            this.ride.setCreate()
        })

        addPrevent(this.btnSearch, this.list.bind(this))

        flatpickr(this.inputFilters.scheduled_begin, {
            dateFormat: "d/m/Y",
        })
        flatpickr(this.inputFilters.scheduled_end, {
            dateFormat: "d/m/Y",
        })

        this.getChoices()
    }

    getChoices() {
        let me = this
        ax.get('/api/driver/').then(response => {
            let drivers = response.data.map(driver => {
                return { label: `${driver.first_name} ${driver.last_name}`, value: driver.id }
            })
            me.inputFilters.driver.setChoices(drivers)
            me.inputRide.driver.setChoices(drivers)
        })
        ax.get('/api/company/').then(response => {
            let companies = response.data.map(company => {
                return { label: company.name, value: company.id };
            })
            me.inputFilters.company.setChoices(companies)
            me.inputRide.company.setChoices(companies)
        })
        ax.get('/api/fleet/').then(response => {
            let cars = response.data.map(car => {
                return { label: `${car.model} ${car.plate} / ${car.capacity}`, value: car.id };
            })
            me.inputRide.car.setChoices(cars)
        })
        ax.get('/api/ride/rate').then(response => {
            let rates = response.data.map(rate => {
                return { label: `${rate.code} - ${rate.price}`, value: rate.code };
            })
            me.inputDrift.rate_code.setChoices(rates)
        })
    }

    getPagination(total) {
        let me = this
        let page = me.page
        let page_size = me.page_size
        let previous = n => `<li class="page-item">
            <a class="page-link previous" data-id="${n}">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>`
        let next = n => `
          <li class="page-item">
            <a class="page-link next" data-id="${n}">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        `
        let numbers = (n, c) => `
          <li class="page-item ${n == c ? 'active' : ''}"><a class="page-link number " data-id="${n}">
          ${n == c ? `<span class="sr-only">(current)</span>` : ''}
          ${n}</a></li>
        `
        let acc = ''
        if (page - 1 > 1) acc += numbers(page - 1, page)
        acc += numbers(page, page)
        if (page + 1 <= total / page_size) acc += numbers(page + 1, page)
        document.querySelector('.pagination').innerHTML = `
          ${page == 1 ? '' : previous(page - 1)}
          ${acc}
          ${page * page_size >= total ? '' : next(page + 1)}
        `
        document.querySelectorAll('.page-link').forEach(a => {
            a.addEventListener('click', () => {
                me.page = parseInt(a.getAttribute('data-id'))
                me.list()
            })
        })
    }
    getListUrl() {
        function getDate(value, func) {
            if (value != "") return func(value).slice(0, -3)
            return value
        }
        let me = this
        let params = {
            scheduled_begin: getDate(me.inputFilters.scheduled_begin.value, date2ts),
            scheduled_end: getDate(me.inputFilters.scheduled_end.value, date2ts),
            company: me.inputFilters.company.getValue(true),
            driver: me.inputFilters.driver.getValue(true),
            page: me.page,
            page_size: me.page_size,
        }
        let strParams = ''
        for (let [param, value] of Object.entries(params)) strParams += value != "" ? `&${param}=${value}` : ''
        return me.api.list + "?" + strParams.slice(1)
    }

    list() {
        let me = this
        return ax.get(me.getListUrl())
            .then(function (response) {
                let tableBody = document.getElementById("tableBody")
                let rows = ''
                response.data.results.forEach((element, index) => {
                    let columns = `<tr>`
                    element['index'] = ++index
                    me.rowRender.forEach(f => {
                        columns += `<td>${f(element)}</td>`
                    })
                    columns += '</tr>'
                    rows += columns
                })
                tableBody.innerHTML = rows
                document.querySelectorAll('.ride-update').forEach(button => {
                    addPrevent(button, me.retrieve.bind(me))
                })
                document.querySelectorAll('.ride-delete').forEach(button => {
                    addPrevent(button, me.delete.bind(me))
                })
                me.getPagination(response.data.count)
            })
        .catch(function (error) {
            swal.fire({ text: 'No se pudo obtener los viajes', icon: 'error' })
        })
    }
    retrieve(id) {
        let me = this
        return me.ride.setUpdate(id)
    }


    delete(id) {
        let me = this
        return alertDelete('¿Estás seguro de eliminar?').then(result => {
            if (result.value) ax.delete(me.api.delete + id)
                .then(function () {
                    me.list()
                    swal.fire({
                        icon: 'success',
                        timer: 600
                    })
                }).catch(function () {
                    swal.fire({
                        icon: 'error',
                        timer: 600,
                        text: 'No se pudo eliminar'
                    })
                })
        })
    }
}


document.addEventListener("DOMContentLoaded", function () {
    let options = {
        api: api.ride,
        rowRender: [
            data => { return data.slug },
            data => { return `${data.driver.first_name} ${data.driver.last_name}` },
            data => { return `${ts2datetime(data.scheduled_time)}` },
            data => { return `${data.place.distrito}` },
            data => { return RIDE_STATUS_CHOICES[data.status]},
            data => {
                return `<button type="button" class="btn btn-primary ride-update" data-id=${data.id}>Editar</button>
                        <button type="button" class="btn btn-danger ride-delete" data-id=${data.id}>Eliminar</button>`
            },
        ]
    }
    let rideManager = new RideManager(options)
    rideManager.list()
});
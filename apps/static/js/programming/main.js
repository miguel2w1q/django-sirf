class Observable {
    constructor() {
        this.data = arguments[0]
        this.alert = arguments[1]
    }

    update(key, val) {
        this.data[key] = val
        this.alert()
    }
}

class ProgrammingManager {
    constructor() {
        this.api = arguments[0].api
        this.companySelector = document.getElementById(arguments[0].companySelector)
        this.fileContainer = document.getElementById(arguments[0].fileContainer)
        this.fileExcel = document.getElementById(arguments[0].fileSelector)
        this.shifts = document.getElementById(arguments[0].shifts)
        this.drifts = document.getElementById(arguments[0].drifts)
        this.drivers = document.getElementById(arguments[0].drivers)
        this.containerFinishProgramming = document.getElementById('container-finish-programming')
        this.btnUpdate = document.getElementById(arguments[0].btnUpdate)
        this.btnRide = document.getElementById('btnRide')
        this.btnClean = document.getElementById('btnClean')
        this.btnUpdateDrift = document.getElementById('btnUpdateDrift')
        this.savedDrifts = null
        this.formModalElement = document.getElementById("formModal")
        this.formModal = new mdb.Modal(this.formModalElement)
        this.formModalElement.addEventListener('shown.bs.modal', fixNotch)
        this.input = {
            firstName: document.getElementById('inputFirstName'),
            lastName: document.getElementById('inputLastName'),
            address: document.getElementById('inputAddress'),
            reference: document.getElementById('inputReference'),
            distrito: document.getElementById('inputDistrito'),
            rate: document.getElementById('inputRate'),
        }
        addPrevent(this.btnClean, this.clean_programming.bind(this))
        addPrevent(this.btnUpdateDrift, this.update_drift.bind(this))
        addPrevent(this.btnRide, this.create_ride.bind(this))
        this.load_companies()
        this.companySelector.addEventListener('change', () => this.load_shifts())
        this.fileExcel.addEventListener('change', () => document.getElementById('lblFileExcel').innerText = this.fileExcel.value)

        this.btnUpdate.addEventListener('click', () => this.load_file())
        this.ride = new Observable({
            driver: null,
            capacity: 100,
            programming: [],
            ds: null,
            btnRide: this.btnRide
        }, function () {
            let data = this.data
            let programming = data.programming
            data.btnRide.style.display = 'none'
            if (programming.length > 1 && !programming.every(p => p.type === programming[0].type)) {
                data.programming = []
                data.ds.setSelection(null)
                swal.fire({ text: `Escoger programaciones del mismo tipo`, icon: 'error' })
            }
            if (programming.length > data.capacity) {
                data.programming = []
                data.ds.setSelection(null)
                swal.fire({ text: `Capacidad maxima: ${data.capacity}`, icon: 'error' })
            } else if (data.programming.length > 0) {
                if (data.driver) data.btnRide.style.display = 'block'
            }
        })
    }

    clean_programming() {
        let me = this
        return ax.get(me.api.programming.clean + me.companySelector.value).then((response) => {
            me.load_shifts()
        })
    }

    clean_ride() {
        let me = this
        me.ride.update('driver', null)
        me.ride.update('programming', [])
    }

    create_ride() {
        let me = this
        let formData = new FormData
        formData.append("driver", me.ride.data.driver)
        formData.append("programming", me.ride.data.programming.map(p => p.id))
        return ax.post(api.programming.ride, formData).then(() => {
            swal.fire({ text: 'Viaje Creado con éxito', icon: 'success' })
            me.load_shifts()
        }).catch(() => {
            swal.fire({ text: 'Error al crear', icon: 'error' })
        })
    }

    load_file() {
        let me = this
        let formData = new FormData()
        formData.append("file", me.fileExcel.files[0])
        formData.append("company_id", me.companySelector.value)
        ax.put(api.programming.file, formData).then(response => {
            me.load_shifts()
        }).catch(error => {
            swal.fire({ text: 'Error con el archivo', icon: 'error' })
        })
    }

    load_companies() {
        let me = this
        ax.get(me.api.company.list).then((response) => {
            const optionReducer = (accummulator, data) => `${accummulator}<option value="${data.id}">${data.name}</option>`
            me.companySelector.innerHTML = response.data.reduce(optionReducer, '')
            me.load_shifts()
        })
    }

    load_shifts() {
        let me = this
        let url = `${api.programming.shift}${me.companySelector.value}`
        me.clean_ride()
        ax.get(url).then((response) => {
            let shifts = response.data.shifts
            let programmed = response.data.programmed
            const shiftReducer = (data) => `
                  <div class="row mb-3 w-100 ">
                      <div class="card ripple shift" data-id="${data.id}">
                        <div class="card-body">
                          <h4 class="card-title mb-0">${ts2datetime(data.scheduled_time)}</span>
                        </div>
                      </div>
                    </div>`
            me.shifts.innerHTML = ''
            me.clean()
            shifts.forEach(e => me.shifts.insertAdjacentHTML('beforeEnd', shiftReducer(e)))
            document.querySelectorAll('.shift')
                .forEach(card => card
                    .addEventListener('click', ev => me.
                        update_shift(card.getAttribute('data-id'))))
            if (shifts.length > 0) {
                if (shifts.some(shift => shift.id == me.lastShiftSelected)) me.update_shift(me.lastShiftSelected)
                else me.update_shift(shifts[0].id)
                me.fileContainer.classList.add('hide')
            } else {
                if (programmed > 0) me.load_finish_button(programmed)
                me.fileContainer.classList.remove('hide')
            }
        })
    }
    load_finish_button(number) {
        let me = this
        me.containerFinishProgramming.innerHTML = `<button type="button" class="btn btn-primary" id="btnFinish">Finalizar Programación</button>`
        document.getElementById('btnFinish').addEventListener('click', () => {
            swal.fire({
                icon: 'question',
                text: `¿Estás seguro de finalizar la programación? tienes ${number} viajes programados.`,
                showCloseButton: true,
                showCancelButton: true,
            }).then(result => {
                if (result.value)
                    ax.get(me.api.programming.finish + me.companySelector.value
                    ).then(() => alertOk('Se te enviará un correo con la información programada') && me.clean()
                    ).catch(() => alertError())
            })
        })

    }
    load_drivers(data) {
        let me = this
        const driverReducer = (data) => `
                  <div class="row mb-3 w-100 ">
                      <div class="card ripple driver" data-id="${data.username}" data-capacity="${data.capacity}">
                        <div class="card-body">
                          <h4 class="card-title mb-0"><span class="badge bg-success">${data.capacity}</span>
                          ${data.last_name}, ${data.first_name}</h4>
                        </div>
                      </div>
                    </div>`
        data.forEach(e => me.drivers.insertAdjacentHTML('beforeEnd', driverReducer(e)))
        let qsDrivers = document.querySelectorAll('.driver')
        qsDrivers.forEach(driver => {
            driver.addEventListener('click', () => {
                me.ride.update('driver', driver.getAttribute('data-id'))
                me.ride.update('capacity', driver.getAttribute('data-capacity'))
                qsDrivers.forEach(driver => driver.classList.remove('ds-selected'))
                driver.classList.add('ds-selected')
            })
        })
    }
    load_drifts(data) {
        let me = this
        me.clean_ride()
        const driftReducer = (data) => `
                <div class="row mb-3 w-100 ">
                    <div class="card ripple drift" data-id='${data.id}' ride-type='${data.ride_type}'>
                        <div class="card-body row">
                            <h4 class="card-title mb-0 col-8">
                            <span class="badge ${data.ride_type ==0?'bg-info':'bg-warning'}">${data.ride_type == 0 ? 'I' : 'S'}</span>
                            <span class="badge bg-secondary">${data.place.rate}</span>
                            ${data.place.distrito} - ${data.place.address}</h4>
                            <div class="drift-options col-4">
                              <span class="bt bt-lg bt-trash mx-1 float-right"></span>
                              <span class="bt bt-lg bt-pencil mx-1 float-right"></span>
                              <span class="bt bt-lg bt-info mx-1 float-right"></span>
                            </div>
                        </div>
                    </div>
                </div>`
        me.savedDrifts = data
        data.forEach(e => me.drifts.insertAdjacentHTML('beforeEnd', driftReducer(e)))
        document.querySelectorAll('.drift').forEach(drift => {
            drift.querySelector('.bt-trash').addEventListener('click', () => {
                me.delete_drift(drift.getAttribute('data-id'))
            })
            drift.querySelector('.bt-pencil').addEventListener('click', () => {
                me.edit_drift(drift.getAttribute('data-id'))
            })
            drift.querySelector('.bt-info').addEventListener('click', () => {
                me.edit_drift(drift.getAttribute('data-id'))
            })
        })
        me.ride.update('ds', new DragSelect({
            selectables: document.getElementsByClassName('drift'),
            area: document.getElementById('drifts'),
            callback: () => me.ride.update('programming', me.ride.data.ds.getSelection().map(drift => {
                return { id: drift.getAttribute('data-id'), type: drift.getAttribute('ride-type') }
            }))
        }));
    }
    edit_drift(id) {
        let me = this
        me.formModal.show()
        me.btnUpdateDrift.setAttribute("data-id", id)
        let drift = me.savedDrifts.find(drift => drift.id == id)
        me.input.firstName.value = drift.passenger.first_name
        me.input.lastName.value = drift.passenger.last_name
        me.input.address.value = drift.place.address
        me.input.reference.value = drift.place.reference
        me.input.distrito.value = drift.place.distrito
        me.input.rate.value = drift.place.rate
    }
    getDriftEntity() {
        let me = this
        return {
            address: me.input.address.value,
            reference: me.input.reference.value,
            distrito: me.input.distrito.value,
            rate: me.input.rate.value,
        }
    }
    update_drift(id) {
        let me = this
        return ax.put(me.api.programming.update + id, me.getDriftEntity())
            .then(function (response) {
                me.formModal.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }).catch(function (error) {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo actualizar'
                })
            })

    }
    delete_drift(id) {
        let me = this
        swal.fire({
            title: '¿Estás seguro de eliminar?',
            icon: 'warning',
            showConfirmButtton: true,
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                ax.delete(me.api.programming.remove + id)
                    .then(function (response) {
                        me.clean_drift(id)
                        swal.fire({
                            icon: 'success',
                            timer: 600
                        })
                    }).catch(function () {
                        swal.fire({
                            icon: 'error',
                            timer: 600,
                            text: 'No se pudo eliminar'
                        })
                    })
            }
        })
    }

    clean_drift(id) {
        let me = this
        document.querySelector(`.drift[data-id="${id}"]`).remove()
        me.savedDrifts = me.savedDrifts.filter(drift => drift.id != id)
        if (me.savedDrifts.length == 0) me.load_shifts()
    }
    clean() {
        let me = this
        me.drivers.innerHTML = ''
        me.drifts.innerHTML = ''
        me.containerFinishProgramming.innerHTML = ''
        me.savedDrifts = null
    }
    update_shift(id) {
        let me = this
        me.lastShiftSelected = id
        document.querySelectorAll('.shift').forEach(drift => drift.classList.remove('ds-selected'))
        document.querySelector(`.shift[data-id="${id}"]`).classList.add('ds-selected')
        ax.get(me.api.programming.data + id).then((response) => {
            me.clean()
            me.load_drifts(response.data.drifts)
            me.load_drivers(response.data.drivers)
        })
    }
}
document.addEventListener("DOMContentLoaded", function () {
    swal.showLoading()
    new ProgrammingManager({
        api: api,
        companySelector: 'companySelector',
        dateSelector: 'dateSelector',
        fileSelector: 'fileExcel',
        fileContainer: 'fileContainer',
        drifts: 'drifts',
        drivers: 'drivers',
        shifts: 'shifts',
        btnUpdate: 'btnUpdate',
    })
});

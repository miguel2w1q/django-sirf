const CSRF_TOKEN = document.getElementsByName("csrfmiddlewaretoken")[0].value
const ax = axios.create({
    headers: { "X-CSRFTOKEN": CSRF_TOKEN }
})
const swal = Sweetalert2.mixin({
    customClass: {
        confirmButton: "btn btn-success m-auto",
        cancelButton: "btn btn-white m-auto",
    },
    buttonsStyling: false,
    reverseButtons: true,
    showCloseButton: true,
    confirmButtonText: "OK",
    cancelButtonText: "Cancelar",
});

ax.interceptors.request.use(function (config) {
    swal.fire({
        title: 'Cargando',
        showCloseButton: false,
        buttonsStyling: true,
        onBeforeOpen: () => {
            swal.showLoading()
        }
    })
    return config;
}, function (error) {
    return Promise.reject(error);
});

ax.interceptors.response.use(function (response) {
    if (response.config.method == "get") swal.close()
    return response;
}, function (error) {
    return Promise.reject(error);
});

const ts2time = unix => moment.unix(unix).format('HH:mm')
const ts2datetime = unix => moment.unix(unix).format('DD/MM - HH:mm')
const date2ts = date => moment(date, 'DD/MM/YYYY').format('x')

const addPrevent = (btn, func, event = 'click') => {
    btn.addEventListener(event, function () {
        if (!btn.getAttribute('disabled')) {
            btn.setAttribute('disabled', true)
            let id = btn.getAttribute('data-id')
            if (id)
                func(id).then(() => { btn.removeAttribute('disabled') })
            else
                func().then(() => { btn.removeAttribute('disabled') })
        }
    })
}

const fixNotch = () => {
    document.querySelectorAll('.form-outline').forEach(fo => {
        let input = fo.querySelector('input')
        if (input.val != '') input.classList.add('active')
        else input.classList.remove('active')
        let lblWidth = fo.querySelector('.form-label').offsetWidth
        fo.querySelector('.form-notch-middle').style.width = `${.8 * lblWidth + 8}px`
    })
}

const createCombo = (data, val, id = '', extra_cls = '') => {
    const optionsReducer = (accumulator, data) => `<option value="${data.id}" ${data.id == val ? "selected" : ''}">${data.txt}</option>${accumulator}`
    let newData = data
    if (!Array.isArray(data)) newData = Object.entries(newData).map(a => {
        if (a[0] === "null") a[0] = null
        return { id: a[0], txt: a[1] }
    })
    return `<select data-id="${id}" class="form-select ${extra_cls}">${newData.reduce(optionsReducer, '')}</select>`
}

const createDataList = (id, url, key) => {
    const optionsReducer = (accumulator, data) => `<option value="${data[key]}" ></option>${accumulator}`
    ax.get(url).then(response => {
        document.getElementById(id).innerHTML = response.data.reduce(optionsReducer, '')
    })
}


const alertError = (msg = 'Error') => {
    return swal.fire({ text: msg, icon: 'error' })
}

const alertOk = (msg = 'Exito') => {
    return swal.fire({ text: msg, icon: 'success' })
}

const alertDelete = (msg = 'Alerta') => {
    return swal.fire({
        title: msg,
        icon: 'warning',
        showCancelButton: true,
    })
}

const simpleChoice = id => new Choices(`#${id}`, {
    removeItemButton: true,
    searchPlaceholderValue: "Escoge una Compañía",
    loadingText: 'Cargando...',
    noResultsText: 'No se encontraron resultados',
    noChoicesText: 'No hay opciones',
    itemSelectText: 'Presiona para elegir',
})

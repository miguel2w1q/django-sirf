class CompanyManager {
    constructor() {
        this.apis = arguments[0].apis
        this.rowRender = arguments[0].rowRender
        this.btnSubmit = document.getElementById("btnSubmit")
        this.formModalElement = document.getElementById("formModal")
        this.formModal = new mdb.Modal(this.formModalElement)
        addPrevent(this.btnSubmit, this.getAction.bind(this))
        document.getElementById("createBtnModal").addEventListener('click', () => { 
            this.btnSubmit.removeAttribute('data-id')
            this.btnSubmit.setAttribute('action', 'create')
            this.formModal.show()
         })
        this.formModalElement.addEventListener('shown.bs.modal', fixNotch)
    }

    list() {
        let me = this
        ax.get(me.apis.list)
            .then(function (response) {
                let tableBody = document.getElementById("tableBody")
                let rows = ''
                response.data.forEach((element, index) => {
                    let columns = `<tr>`
                    element['index'] = ++index
                    me.rowRender.forEach(f => {
                        columns += `<td>${f(element)}</td>`
                    })
                    columns += '</tr>'
                    rows += columns
                })
                tableBody.innerHTML = rows
                document.querySelectorAll("button.update").forEach(button => {
                    addPrevent(button, me.retrieve.bind(me))
                })
                document.querySelectorAll("button.delete").forEach(button => {
                    addPrevent(button, me.delete.bind(me))
                })
            }).catch(function (error) {
                swal.fire({ text: 'No se pudo obtener las Compañías', icon: 'error' })
            })
    }

    getEntity() {
        return {
            name: document.getElementById("inputName").value,
            address: {
                address: document.getElementById("inputAddress").value,
                reference: document.getElementById("inputReference").value,
            },
            email: document.getElementById("inputEmail").value 
        }
    }

    setEntity(data) {
        document.getElementById("inputName").value = data.name
        document.getElementById("inputAddress").value = data.address.address
        document.getElementById("inputReference").value = data.address.reference
        document.getElementById("inputEmail").value = data.email
    }

    clearEntity() {
        document.getElementById("inputName").value = ''
        document.getElementById("inputAddress").value = ''
        document.getElementById("inputReference").value = ''
        document.getElementById("inputEmail").value = ''
    }

    getAction(id) {
        let me = this
        if (me.btnSubmit.getAttribute("action") == "update") return me.update(id)
        else return me.create()
    }

    retrieve(id) {
        let me = this
        me.btnSubmit.setAttribute("action", "update")
        me.btnSubmit.setAttribute("data-id", id)
        return ax.get(me.apis.retrieve + id)
            .then(function (response) {
                me.setEntity(response.data)
                me.formModal.show()
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
            })
    }

    update() {
        let me = this
        let id = me.btnSubmit.getAttribute('data-id')
        return ax.put(me.apis.update + id, me.getEntity())
            .then(function (response) {
                me.list()
                me.clearEntity()
                me.formModal.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }).catch(function (error) {
                swal.fire(error)
            })
    }

    create() {
        let me = this
        return ax.post(me.apis.create, me.getEntity())
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
                me.formModal.hide()
                me.clearEntity()
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }

    delete(id) {
        let me = this
        return ax.delete(me.apis.delete + id)
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let options = {
        apis: api.company,
        rowRender: [
            data => { return data.index },
            data => { return data.name },
            data => { return data.address.address },
            data => { return data.address.reference },
            data => { return data.email },
            data => {
                return `<button type="button" class="btn btn-primary update" data-id=${data.id}>Editar</button>
                              <button type="button" class="btn btn-danger delete" data-id="${data.id}">Eliminar</button>`
            },
        ]
    }
    let carManager = new CompanyManager(options)
    carManager.list()
});
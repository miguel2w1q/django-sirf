DRIFT_NO_PICKED = 0
DRIFT_PICKED = 1
DRIFT_EMERGENCY = 2
DRIFT_ASK_HELP = 3
DRIFT_STATUS_CHOICES = {
    [DRIFT_PICKED]:"Recogido",
    [DRIFT_NO_PICKED]: "No recogido",
    [DRIFT_EMERGENCY]: "Emergencia",
    [DRIFT_ASK_HELP]: "Solicita Ayuda",
}

RIDE_PENDING = 0
RIDE_ON_GOING = 1
RIDE_FINISHED = 2

RIDE_STATUS_CHOICES = {
    [RIDE_ON_GOING]:"En camino",
    [RIDE_FINISHED]: "Terminado",
    [RIDE_PENDING]: "Pendiente",
}

NO_ACTION = null
ACCEPTED = 0
DENIED = 1
DRIFT_OPERATOR_STATUS_CHOICES = {
    [NO_ACTION]:"Sin desición",
    [ACCEPTED]: "Aceptado",
    [DENIED]: "Denegado",
}
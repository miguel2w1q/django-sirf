class Manager {
    constructor() {
        this.apis = arguments[0].apis
        this.rowRender = arguments[0].rowRender
        this.forms = arguments[0].forms
        this.modals = arguments[0].modals
        let btnCreate = arguments[0].saveButtons.create
        addPrevent(btnCreate, this.create.bind(this))
        this.btnUpdate = arguments[0].saveButtons.update
        addPrevent(this.btnUpdate, this.update.bind(this))
    }

    list() {
        let me = this
        ax.get(me.apis.list)
            .then(function (response) {
                let tableBody = document.getElementById("tableBody")
                let rows = ''
                response.data.forEach((element, index) => {
                    let columns = `<tr>`
                    element['index'] = ++index
                    me.rowRender.forEach(f => {
                        columns += `<td>${f(element)}</td>`
                    })
                    columns += '</tr>'
                    rows += columns
                })
                tableBody.innerHTML = rows
                document.querySelectorAll("button.update").forEach(button => {
                    addPrevent(button, me.retrieve.bind(me))
                })
                document.querySelectorAll("button.delete").forEach(button => {
                    addPrevent(button, me.delete.bind(me))
                })
            }).catch(function (error) {
                swal.fire({ text: 'No se pudo obtener los vehiculos', icon: 'error' })
            })
    }

    retrieve(id) {
        let me = this
        me.btnUpdate.setAttribute('data-id', id)
        return ax.get(me.apis.retrieve + id)
            .then(function (response) {
                me.forms.update.getElementsByTagName('input').forEach(input => {
                    input.value = response.data[input.getAttribute('name')]
                    if (input.value != '') input.classList.add('active')
                })
                me.modals.update.show()
            }).catch(function (error) {
                swal.fire({ text: 'Problemas al obtener la informacion', icon: 'error' })
            })
    }

    update() {
        let me = this
        let formData = new FormData(me.forms.update)
        let id = me.btnUpdate.getAttribute('data-id')
        return ax.put(me.apis.update + id, formData)
            .then(function (response) {
                me.list()
                me.modals.update.hide()
                swal.fire({ text: 'Actualizado', icon: 'success' })
            }).catch(function (error) {
                swal.fire(error)
            })
    }

    create() {
        let me = this
        let formData = new FormData(me.forms.create)
        return ax.post(me.apis.create, formData)
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
                me.modals.create.hide()
                me.forms.create.getElementsByTagName('input').forEach(input => input.value = '')
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }

    delete(id) {
        let me = this
        return ax.delete(me.apis.delete + id)
            .then(function (response) {
                me.list()
                swal.fire({
                    icon: 'success',
                    timer: 600
                })
            }).catch(function () {
                swal.fire({
                    icon: 'error',
                    timer: 600,
                    text: 'No se pudo crear'
                })
            })
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let options = {
        apis: api.fleet,
        rowRender: [
            data => { return data.index },
            data => { return data.brand },
            data => { return data.model },
            data => { return data.plate },
            data => { return data.capacity },
            data => {
                return `<button type="button" class="btn btn-primary update" data-id=${data.id}>Editar</button>
                              <button type="button" class="btn btn-danger delete" data-id="${data.id}">Eliminar</button>`
            },
        ],
        forms: {
            create: document.getElementById("formCreate"),
            update: document.getElementById("formUpdate")
        },
        saveButtons: {
            create: document.getElementById("btnCreate"),
            update: document.getElementById("btnUpdate")
        },
        modals: {
            create: new mdb.Modal(document.getElementById("createFormModal")),
            update: new mdb.Modal(document.getElementById("updateFormModal"))
        }

    }
    let carManager = new Manager(options)
    carManager.list()
});
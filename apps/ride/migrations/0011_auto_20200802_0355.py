# Generated by Django 3.0.5 on 2020-08-02 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0010_programming_ride_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='ride',
            name='finished_latitude',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Finished Latitude'),
        ),
        migrations.AddField(
            model_name='ride',
            name='finished_longitude',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True, verbose_name='Finished Longitude'),
        ),
        migrations.AddField(
            model_name='ride',
            name='finished_time',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Finished Time'),
        ),
        migrations.AddField(
            model_name='ride',
            name='slug',
            field=models.SlugField(max_length=10, null=True),
        ),
    ]

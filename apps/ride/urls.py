from apps.ride.views import (
    CompanyPanelView,
    DriverPanelView,
    EmergencyPanelView,
    FleetPanelView,
    ProgramingPanelView,
    RidePanelView,
    RideSearchView,
    RideDetailView,
)
from django.urls import path

urlpatterns = [
    path('', ProgramingPanelView.as_view(), name="home"),
    path('panel/programming', ProgramingPanelView.as_view(), name='programming'),
    path('panel/driver', DriverPanelView.as_view(), name='driver'),
    path('panel/fleet', FleetPanelView.as_view(), name='fleet'),
    path('panel/company', CompanyPanelView.as_view(), name='company'),
    path('panel/ride', RidePanelView.as_view(), name='ride'),
    path('panel/emergency', EmergencyPanelView.as_view(), name='emergency'),
    path('vale/', RideSearchView.as_view(), name='ride_search'),
    path('vale/<str:ride_code>', RideDetailView.as_view(), name='ride_detail'),
]

app_name = 'panel'

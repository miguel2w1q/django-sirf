import django_rq
from datetime import time, timedelta
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand
from apps.ride.models import Programming, Ride, Drift
from apps.common.utils import ExcelFile, add_tz
from django.utils.timezone import localtime, now
from apps.common.constants import RIDE_TYPE_GOING, RIDE_CREATION_TYPE_MANUAL
from rq_scheduler import Scheduler
from redis import Redis

header_color = "900C3F"
row_color_1 = "DAF7A6"
row_color_2 = "FFF9D8"
white_color = "FFFFFF"
black_color = "000000"

def get_report(query_set, showCost=True):
    rides = list(query_set)
    groups = []
    manual = []
    night_shifts = []
    day_shifts = []
    programmed = []
    seven_pm = time(19, 0, 0)
    seven_am = time(7, 0, 0)
    for ride in rides:
        if ride.creation_type == RIDE_CREATION_TYPE_MANUAL:
            manual.append(ride)
        else:
            programmed.append(ride)
    groups.append({'name': 'Transporte de Personal Staff', 'rides': manual})
    for ride in programmed:
        ride_time = ride.scheduled_time.time()
        if ride_time < seven_am and seven_pm < ride_time:
            night_shifts.append(ride)
        else:
            day_shifts.append(ride)
    groups.append({'name': 'Transporte de Personal Diurno', 'rides': night_shifts})
    groups.append({'name': 'Transporte de Personal Nocturno', 'rides': day_shifts})

    excel = ExcelFile("Reporte")
    for group in groups:
        excel.change_ws(group['name'])
        headers = [
            {'text': 'VALE', 'width': 8},
            {'text': 'FECHA', 'width': 8},
            {'text': 'SERVICIO', 'width': 8},
            {'text': 'INGRESO', 'width': 8},
            {'text': 'CONDUCTOR', 'width': 8},
            {'text': 'DNI', 'width': 8},
            {'text': 'TRABAJADOR', 'width': 45},
            {'text': 'DIRECCION', 'width': 45},
            {'text': 'REFERENCIA', 'width': 45},
            {'text': 'DISTRITO', 'width': 12},
            {'text': 'TELEFONO', 'width': 12},
            {'text': 'COB', 'width': 8},
            {'text': 'TAR', 'width': 8},
            {'text': 'HORA LLEGADA', 'width': 8},
            {'text': 'TARDANZA', 'width': 8},
            {'text': 'ASISTENCIA', 'width': 8},
            {'text': 'OBSERVACION', 'width': 12},
        ]
        excel.add_headers(2, 2, headers)
        cost_col_start = 2 + 2 + len(headers)
        if showCost:
            rateHeaders = [
                {'text': 'SUBTOTAL', 'width': 8},
                {'text': 'PENALIDAD', 'width': 8},
                {'text': 'TOTAL', 'width': 8},
            ]
            excel.add_headers(cost_col_start, 2, rateHeaders)

        row = 3
        row_color_counter = 0
        for ride in group['rides']:
            scheduled_time = localtime(ride.scheduled_time)
            arrival_time = localtime(ride.arrival_time) if  ride.arrival_time else None
            ride_type = 'INGRESO' if ride.type == RIDE_TYPE_GOING else 'SALIDA'
            driver = ride.driver.full_name
            row_color = row_color_1 if row_color_counter % 2 == 0 else row_color_2
            row_color_counter += 1
            for drift in ride.drift_set.all():
                place = drift.pickup_place
                data = [
                    ride.slug,
                    localtime(ride.scheduled_time).strftime("%d/%m/%Y"),
                    ride_type,
                    scheduled_time.time().strftime("%H:%M"),
                    driver,
                    drift.passenger.username,
                    drift.passenger.full_name,
                    place.address,
                    place.reference,
                    place.secondary_distrito,
                    ' - '.join(drift.passenger.phones),
                    place.rate.code,
                    f"S/ {place.rate.price}",
                    localtime(ride.arrival_time).strftime("%H:%M") if ride.arrival_time else '',
                    (scheduled_time - arrival_time).strtime("%H:%M") if arrival_time and arrival_time > scheduled_time else '',
                    'Sí' if drift.pickup_time else 'No',
                    drift.OBSERVACION
                ]
                excel.row(2, row, data, row_color, black_color)
                if showCost:
                    data_cost = [
                        ride.sub_total,
                        ride.discount,
                        ride.total,
                    ] 
                    excel.row(cost_col_start, row, data_cost, row_color, black_color)
                row += 1
        excel.send()

def daily_report():
    qs = Ride.objects.filter(
            scheduled_time__range=[localtime()-timedelta(days=1), localtime()]
            ).prefetch_related('drift_set')
    django_rq.enqueue(get_report, qs, False)


def biweekly_report(day):
    if day == 1:
        start_time = (localtime()-relativedelta(months=1)).replace(days=15)
        finish_time = localtime()
    elif day == 15:
        start_time = localtime()-relativedelta(days=15)
        finish_time = localtime()
    else:
        raise Exception("Incorrect day for job")
    qs = Ride.objects.filter(
            scheduled_time__range=[start_time, finish_time]
            ).prefetch_related('drift_set')
    django_rq.enqueue(get_report, qs)


def monthly_report():
    qs = Ride.objects.filter(
            scheduled_time__range=[localtime()-timedelta(months=1), localtime()]
            ).prefetch_related('drift_set')
    django_rq.enqueue(get_report, qs)


class Command(BaseCommand):
    help = 'Generate report'

    def handle(self, **kwargs):
        scheduler = Scheduler(connection=Redis())
        # Clean Jobs 
        for job in scheduler.get_jobs():
            scheduler.cancel(job)
        scheduler.cron(
                "0 0 * * *",
                func=daily_report,
                use_local_timezone=True)
        scheduler.cron(
                "0 0 1 * *",
                func=biweekly_report,
                args=[1],
                use_local_timezone=True)
        scheduler.cron(
                "0 0 15 * *",
                func=biweekly_report,
                args=[15],
                use_local_timezone=True)
        scheduler.cron(
                "0 0 1 * *",
                func=monthly_report,
                use_local_timezone=True)

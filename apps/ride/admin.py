from apps.ride.models import Car, Company, Drift, Photo, Place, Programming, Rate, Ride
from django.contrib import admin

admin.site.register(Rate)
admin.site.register(Photo)
admin.site.register(Car)
admin.site.register(Company)
admin.site.register(Ride)
admin.site.register(Drift)
admin.site.register(Programming)
admin.site.register(Place)

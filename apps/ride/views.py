from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, DetailView
from .models import Ride


class DriverPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/driver.html'


class CompanyPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/company.html'


class ProgramingPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/programming.html'


class RidePanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/ride.html'


class FleetPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/fleet.html'


class EmergencyPanelView(LoginRequiredMixin, TemplateView):
    template_name = 'panel/emergency.html'


class RideSearchView(TemplateView):
    template_name = 'ride/search.html'


class RideDetailView(DetailView):
    template_name = 'ride/detail.html'
    queryset = Ride.objects.all()
    slug_url_kwarg = 'ride_code'
    slug_field = 'slug'

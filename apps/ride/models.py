import datetime

import googlemaps
from apps.common.constants import (
    DRIFT_OPERATOR_STATUS_CHOICES,
    DRIFT_STATUS_CHOICES,
    NO_PICKED,
    PICKED,
    FINISHED,
    ON_GOING,
    PENDING,
    RIDE_TYPE_CHOICES,
    RIDE_TYPE_GOING,
    RIDE_TYPE_RETURN,
    RIDE_CREATION_TYPE_CHOICES,
    RIDE_CREATION_TYPE_PROGRAMMED
)
from apps.common.models import AuditableModel, TimeStampModel
from apps.common.utils import add_tz
from apps.location.models import Distrito
from apps.users.models import User
from django.conf import settings
from django.db import models
from django.db.models import Q

gmaps = googlemaps.Client(key=settings.GOOGLE_MAPS_KEY)


class Rate(TimeStampModel, AuditableModel):
    code = models.CharField('Code', max_length=5, unique=True)
    price = models.DecimalField('Price', max_digits=10, decimal_places=2, default=40.00)

    def __str__(self):
        return self.code


class Photo(TimeStampModel):
    created_time = models.DateTimeField('Arrival Time', blank=False, null=True)
    base64 = models.TextField('Foto', null=True)
    latitude = models.DecimalField('Latitude', max_digits=9, decimal_places=6)
    longitude = models.DecimalField('Longitude', max_digits=9, decimal_places=6)


class Place(TimeStampModel):
    address = models.CharField('Address', max_length=300, blank=True, null=True)
    reference = models.CharField('Reference', max_length=300, blank=True, null=True)
    latitude = models.DecimalField('Latitude', max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField('Longitude', max_digits=9, decimal_places=6, blank=True, null=True)
    distrito = models.ForeignKey(Distrito, on_delete=models.PROTECT, blank=True, null=True)
    secondary_distrito = models.CharField('Secondary Distrito', max_length=50, blank=True, default='')
    rate = models.ForeignKey(Rate, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return f'{self.address} - {self.secondary_distrito}'

    def update_coords(self):
        data = gmaps.geocode('{}, {}'.format(self.address, 'Lima, Peru'))
        if not data:
            return
        data_address = data[0].get('address_components', None)
        data_geometry = data[0].get('geometry', None)
        postal_code = ''
        distrito_name = ''
        for component in data_address:
            types = component.get('types')
            long_name = component.get('long_name')
            if 'postal_code' in types:
                postal_code = long_name
            elif 'locality' in types and 'political' in types:
                distrito_name = long_name
        distrito = Distrito.objects.filter(Q(name=distrito_name) | Q(postal_code=postal_code)).first()
        if not distrito:
            distrito = Distrito.objects.create(postal_code=postal_code, name=distrito_name)
        self.distrito = distrito
        if data_geometry:
            self.latitude = data_geometry.get('location').get('lat')
            self.longitude = data_geometry.get('location').get('lng')
        self.save()

    def update_from_coords(self):
        pass

    @classmethod
    def get_from_address(cls, address):
        place, created = cls.objects.get_or_create(address=address.upper())
        if not created:
            return place
        else:
            place.update_coords()
        return place


class Car(TimeStampModel):
    brand = models.CharField('Car\'s brand', max_length=50, blank=False, null=True)
    model = models.CharField('Car\'s model', max_length=50, blank=False, null=True)
    plate = models.CharField('Plate\'s model ', max_length=10, blank=False, null=True)
    capacity = models.IntegerField('Car\'s capacity', blank=False, null=True)

    def __str__(self):
        return f'{self.model} {self.plate}'

    @classmethod
    def get_car(cls, data):
        car, _ = cls.objects.get_or_create(plate=data.pop('plate'))
        for attr, value in data.items():
            setattr(car, attr, value)
        car.save()
        return car


class Company(TimeStampModel, AuditableModel):
    name = models.CharField('Name', max_length=100)
    email = models.EmailField(blank=True, null=True)
    address = models.ForeignKey(Place, on_delete=models.PROTECT, blank=True,
                                null=True, related_name='company_default_address')
    programming_number = models.IntegerField('Programming quantity', default=1)

    def __str__(self):
        return self.name

    def update_address(self, data):
        place, created = Place.objects.get_or_create(address=data.get('address'))
        if created:
            place.update_coords()
        place.reference = data.get('reference')
        place.save()
        self.address = place
        self.save()


class Ride(TimeStampModel, AuditableModel):
    driver = models.ForeignKey(User, on_delete=models.PROTECT, related_name='rides_as_driver', blank=False, null=True)
    company = models.ForeignKey(Company, on_delete=models.PROTECT, blank=True, null=True)
    scheduled_time = models.DateTimeField('Scheduled Time', blank=True, null=True)
    arrival_photos = models.ManyToManyField(Photo, blank=True)
    arrival_time = models.DateTimeField('Arrival Time', blank=True, null=True)
    arrival_place = models.ForeignKey(Place, on_delete=models.PROTECT, blank=True, null=True)
    arrival_latitude = models.DecimalField('Arrival Latitude', max_digits=9, decimal_places=6, blank=True, null=True)
    arrival_longitude = models.DecimalField('Arrival Longitude', max_digits=9, decimal_places=6, blank=True, null=True)
    started_time = models.DateTimeField('Started Time', blank=True, null=True)
    started_latitude = models.DecimalField('Started Latitude', max_digits=9, decimal_places=6, blank=True, null=True)
    started_longitude = models.DecimalField('Started Longitude', max_digits=9, decimal_places=6, blank=True, null=True)
    finished_time = models.DateTimeField('Finished Time', blank=True, null=True)
    finished_latitude = models.DecimalField('Finished Latitude', max_digits=9, decimal_places=6, blank=True, null=True)
    finished_longitude = models.DecimalField('Finished Longitude', max_digits=9, decimal_places=6, blank=True, null=True)
    rate = models.DecimalField('Rate', max_digits=9, decimal_places=6, blank=True, default=0)
    car = models.ForeignKey(Car, on_delete=models.PROTECT, blank=True, null=True)
    programming_number = models.IntegerField('Programming number', blank=True, null=True)
    type = models.IntegerField('Ride type', choices=RIDE_TYPE_CHOICES, blank=True, default=RIDE_TYPE_GOING)
    creation_type = models.IntegerField('Creation type', choices=RIDE_CREATION_TYPE_CHOICES, blank=True, default=RIDE_CREATION_TYPE_PROGRAMMED)
    slug = models.SlugField(max_length=10, null=True)

    def __str__(self):
        return f"{self.id} {self.driver} {self.rate}"

    def update_coords(self):
        arrival_place = self.arrival_place
        if not arrival_place.latitude or not arrival_place.longitude:
            arrival_place.latitude = self.arrival_latitude
            arrival_place.longitude = self.arrival_longitude
            arrival_place.update_from_coords()

    def compute_rate(self):
        drifts = list(self.drift_set.all())
        goods = [drift for drift in drifts if drift.status == PICKED]
        max_price = 0
        max_drift = None
        counter = 0
        for drift in goods:
            if drift.rate > max_price:
                max_price = drift.rate
                max_drift = drift
            if drift.rate != 0:
                counter += 1
        for drift in drifts:
            if drift != max_drift and drift in goods:
                drift.rate = 4
            else:
                drift.rate = 0
            drift.save()
        self.rate = max_price + 4*counter
        self.save()

    @property
    def status(self):
        if self.type == RIDE_TYPE_GOING:
            if self.started_time:
                return ON_GOING if not self.arrival_time else FINISHED
            else:
                return PENDING
        else:
            if self.finished_time:
                return FINISHED
            else:
                return ON_GOING if self.started_time else PENDING

    @property
    def sub_total(self):
        return self.rate

    @property
    def discount(self):
        return  0.5 if ( self.arrival_time and self.scheduled_time < self.arrival_time) else 0

    @property
    def total(self):
        return  self.sub_total - self.rate * self.discount

    class Meta:
        unique_together = ('scheduled_time', 'driver')


class Drift(TimeStampModel, AuditableModel):
    ride = models.ForeignKey(Ride, on_delete=models.CASCADE, blank=False, null=True, related_name='drift_set')
    passenger = models.ForeignKey(User, on_delete=models.PROTECT,
                                  related_name='drifts_as_passenger', blank=False, null=True)
    pickup_time = models.DateTimeField('Pickup Time', blank=True, null=True)
    pickup_place = models.ForeignKey(Place, on_delete=models.PROTECT, blank=True, null=True)
    pickup_photos = models.ManyToManyField(Photo, blank=True)
    pickup_latitude = models.DecimalField('Latitude', max_digits=9, decimal_places=6, blank=True, null=True)
    pickup_longitude = models.DecimalField('Longitude', max_digits=9, decimal_places=6, blank=True, null=True)
    status = models.IntegerField('Status', choices=DRIFT_STATUS_CHOICES, default=NO_PICKED)
    status_description = models.TextField('Status description', blank=True, null=True)
    operator_status = models.IntegerField(
        'Operator status', choices=DRIFT_OPERATOR_STATUS_CHOICES, blank=True, null=True)
    rate = models.DecimalField('Rate', max_digits=9, decimal_places=6, blank=True, default=0)
    rate_code = models.CharField('Rate Code', max_length=9, blank=True, null=True)
    programming_number = models.IntegerField('Programming number', blank=True, null=True)

    def __str__(self):
        return f"{self.id} {self.passenger} {self.ride}"

    def update_coords(self):
        pickup_place = self.pickup_place
        if not pickup_place.latitude or not pickup_place.longitude:
            pickup_place.latitude = self.pickup_latitude
            pickup_place.longitude = self.pickup_longitude
            pickup_place.update_from_coords()

    def get_rate(self):
        rate = self.pickup_place.rate
        self.rate = rate.price
        self.rate_code = rate.code


class Programming(TimeStampModel):
    scheduled_time = models.DateTimeField('Scheduled Time', blank=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.PROTECT, blank=True, null=True)
    passenger = models.ForeignKey(User, on_delete=models.PROTECT,
                                  related_name='programming_as_passenger', blank=False, null=True)
    place = models.ForeignKey(Place, on_delete=models.PROTECT, blank=True, null=True)
    drift = models.ForeignKey(Drift, on_delete=models.CASCADE, blank=True, null=True, related_name='programming_set')
    ride_type = models.IntegerField('Ride type', choices=RIDE_TYPE_CHOICES, blank=True, default=RIDE_TYPE_GOING)

    class Meta:
        unique_together = ('scheduled_time', 'passenger')

    @classmethod
    def obtain_data_from_excel(cls, data):
        user = User.obtain_from_excel({
            'dni': data.get('dni'),
            'full_name': data.get('full_name'),
            'phones': data.get('phones'),
        })
        rate, _ = Rate.objects.get_or_create(code=data.get('rate_code'))
        ride_type = RIDE_TYPE_GOING if data.get('service') == "INGRESO" else RIDE_TYPE_RETURN
        try:
            place = Place.objects.get(address=data.get('address'))
            if not place.distrito:
                place.secondary_distrito = data.get('distrito')
                place.save()
        except Place.DoesNotExist:
            place = Place.objects.create(
                address=data.get('address'),
                reference=data.get('reference'),
                rate=rate,
                secondary_distrito=data.get('distrito'))
            place.update_coords()
        arrival_time = data.get('arrival_time')
        arrival_time = arrival_time if isinstance(arrival_time, datetime.time) else arrival_time.time()
        scheduled_time = add_tz(datetime.datetime.combine(data.get('arrival_date'), arrival_time))
        if arrival_time > datetime.time(20, 0, 0):
            scheduled_time = scheduled_time - datetime.timedelta(days=1)
        programming_data = {
            'scheduled_time': scheduled_time - datetime.timedelta(minutes=30),
            'company': data.get('company'),
            'passenger': user,
            'ride_type': ride_type,
            'place': place,
        }
        return programming_data

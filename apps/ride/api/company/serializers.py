from apps.ride.models import Company, Place
from rest_framework import serializers


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'place_company_panel'
        model = Place
        fields = ('address', 'reference')


class CompanySerializer(serializers.ModelSerializer):
    address = PlaceSerializer()

    class Meta:
        model = Company
        fields = ('id', 'name', 'address', 'email')

    def create(self, validated_data):
        company = Company.objects.create(name=validated_data.get('name'))
        company.update_address(validated_data.get('address'))
        company.email = validated_data.get('email', '')
        return company

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name')
        instance.email = validated_data.get('email')
        instance.update_address(validated_data.get('address'))
        return instance

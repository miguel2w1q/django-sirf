from django.urls import path

from .views import CompanyCreateListAPIView, CompanyRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', CompanyCreateListAPIView.as_view(), name='list_create'),
    path('', CompanyRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
    path('<int:pk>', CompanyRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
]

app_name = "api_company"

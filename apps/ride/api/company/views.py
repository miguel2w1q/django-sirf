from apps.common.views import NoPatchView
from apps.ride.models import Company
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from .serializers import CompanySerializer


class CompanyCreateListAPIView(ListCreateAPIView):
    queryset = Company.objects.all().select_related('address').order_by('id')
    serializer_class = CompanySerializer


class CompanyRetrieveUpdateDestroyAPIView(NoPatchView, RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

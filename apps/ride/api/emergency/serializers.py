from apps.ride.api.programming.serializers import (
    DriverSerializer,
    PlaceSerializer,
    UserSerializer,
)
from apps.ride.models import Drift
from rest_framework import serializers


class EmergencyListSerializer(serializers.ModelSerializer):
    driver = serializers.SerializerMethodField()
    passenger = serializers.SerializerMethodField()
    place = serializers.SerializerMethodField()

    def get_driver(self, drift):
        return DriverSerializer(drift.ride.driver).data

    def get_passenger(self, drift):
        return UserSerializer(drift.passenger).data

    def get_place(self, drift):
        return PlaceSerializer(drift.pickup_place).data

    class Meta:
        model = Drift
        fields = ('id', 'ride', 'driver', 'passenger', 'status',
                  'status_description', 'operator_status', 'pickup_photos', 'place')


class EmergencyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drift
        fields = ('operator_status',)

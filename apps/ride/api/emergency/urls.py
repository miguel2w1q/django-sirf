from django.urls import path

from .views import EmergencyListAPIView, EmergencyUpdateAPIView

urlpatterns = [
    path('', EmergencyListAPIView.as_view(), name='list'),
    path('', EmergencyUpdateAPIView.as_view(), name='update'),
    path('<int:pk>', EmergencyUpdateAPIView.as_view(), name='update'),
]

app_name = "api_emergency"

from apps.common.constants import ASK_HELP, EMERGENCY
from apps.ride.models import Drift
from rest_framework.generics import ListAPIView, UpdateAPIView

from .serializers import EmergencyListSerializer, EmergencyUpdateSerializer


class EmergencyListAPIView(ListAPIView):
    queryset = Drift.objects.filter(status__in=[EMERGENCY, ASK_HELP], operator_status__isnull=True).prefetch_related('ride', 'ride__driver', 'ride__driver__car','passenger', 'pickup_photos', 'pickup_place')
    serializer_class = EmergencyListSerializer


class EmergencyUpdateAPIView(UpdateAPIView):
    queryset = Drift.objects.all()
    serializer_class = EmergencyUpdateSerializer

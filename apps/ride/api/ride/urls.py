from django.urls import path

from .views import (
    DriftCreateAPIView,
    DriftRetrieveUpdateDestroyAPIView,
    RateListAPIView,
    RideListAPIView,
    RideCreateAPIView,
    RideRetrieveUpdateDestroyAPIView,
)

urlpatterns = [
    path('', RideListAPIView.as_view(), name='list'),
    path('create', RideCreateAPIView.as_view(), name='create'),
    path('<int:pk>', RideRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_update'),
    path('drift/', DriftCreateAPIView.as_view(), name='create_drift'),
    path('drift/<int:pk>', DriftRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
    path('rate', RateListAPIView.as_view(), name='rate_list')
]

app_name = "api_ride"

from apps.ride.api.programming.serializers import DriverSerializer, PlaceSerializer
from apps.ride.models import Drift, Place, Rate, Ride
from apps.users.models import User
from rest_framework import serializers
from apps.common.constants import RIDE_CREATION_TYPE_MANUAL


class RateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = ('id', 'code', 'price')


class DriftListSerializer(serializers.ModelSerializer):
    passenger = serializers.SerializerMethodField()

    def get_passenger(self, drift):
        return drift.passenger.full_name

    class Meta:
        model = Drift
        fields = ('id', 'passenger', 'status', 'rate_code')


class DriftSerializer(serializers.ModelSerializer):
    passenger_dni = serializers.SerializerMethodField()
    passenger_first_name = serializers.SerializerMethodField()
    passenger_last_name = serializers.SerializerMethodField()
    address_name = serializers.SerializerMethodField()


    dni = serializers.CharField(write_only=True, source=None)
    first_name = serializers.CharField(write_only=True, source=None)
    last_name = serializers.CharField(write_only=True, source=None)
    address = serializers.CharField(write_only=True, source=None)
    os = serializers.CharField(write_only=True, source='operator_status', allow_null=True)

    def get_passenger_dni(self, drift):
        return drift.passenger.username

    def get_passenger_first_name(self, drift):
        return drift.passenger.first_name

    def get_passenger_last_name(self, drift):
        return drift.passenger.last_name

    def get_address_name(self, drift):
        return drift.pickup_place.address

    class Meta:
        model = Drift
        fields = ('passenger_dni', 'passenger_first_name', 'passenger_last_name', 'address_name', 'status', 'operator_status',
                  'ride', 'os', 'rate', 'rate_code', 'dni', 'first_name', 'last_name', 'address')

    def update(self, instance, validated_data):
        passenger = {
            'dni': validated_data.pop('dni'),
            'first_name': validated_data.pop('first_name'),
            'last_name': validated_data.pop('last_name')
        }
        place = Place.get_from_address(validated_data.pop('address'))
        data = {
            'passenger': User.obtain_from_drift(passenger),
            'operator_status': validated_data.pop('operator_status'),
            'rate': validated_data.pop('rate'),
            'rate_code': validated_data.pop('rate_code'),
            'pickup_place': place
        }
        for key, val in data.items():
            setattr(instance, key, val)
        instance.save()
        return instance

    def create(self, validated_data):
        passenger = {
            'dni': validated_data.pop('dni'),
            'first_name': validated_data.pop('first_name'),
            'last_name': validated_data.pop('last_name')
        }
        place = Place.get_from_address(validated_data.pop('address'))
        data = {
            'ride': validated_data.pop('ride'),
            'passenger': User.obtain_from_drift(passenger),
            'operator_status': validated_data.pop('operator_status'),
            'rate': validated_data.pop('rate'),
            'rate_code': validated_data.pop('rate_code'),
            'pickup_place': place
        }
        return Drift.objects.create(**data)


class RideListSerializer(serializers.ModelSerializer):
    driver = serializers.SerializerMethodField()
    place = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def get_driver(self, ride):
        return DriverSerializer(ride.driver).data

    def get_place(self, ride):
        return PlaceSerializer(ride.arrival_place).data

    def get_status(self, ride):
        return ride.status

    class Meta:
        model = Ride
        fields = ('id', 'driver', 'place', 'company', 'scheduled_time', 'status', 'slug')


class RideSerializer(serializers.ModelSerializer):
    driver_name = serializers.SerializerMethodField()
    address_name = serializers.SerializerMethodField()
    company_name = serializers.SerializerMethodField()
    car_name = serializers.SerializerMethodField()
    drifts = serializers.SerializerMethodField()
    scheduled_time = serializers.DateTimeField(input_formats=["%d-%m-%Y %H:%M"], format="%d-%m-%Y %H:%M")
    address = serializers.CharField(write_only=True, source=None)

    def get_driver_name(self, ride):
        return ride.driver.full_name

    def get_address_name(self, ride):
        return ride.arrival_place.address

    def get_company_name(self, ride):
        return str(ride.company)

    def get_car_name(self, ride):
        return ride.car.plate

    def get_drifts(self, ride):
        return DriftListSerializer(ride.drift_set.prefetch_related('passenger'), many=True).data

    def create(self, validated_data):
        address = validated_data.pop('address', '')
        if address:
            place = Place.get_from_address(address)
        ride = super().create(validated_data) 
        ride.arrival_place = place
        ride.slug = f"RC-{ride.id}"
        ride.creation_type = RIDE_CREATION_TYPE_MANUAL
        ride.save()
        return ride

    def update(self, instance, validated_data):
        address = validated_data.pop('address', '')
        if address:
            instance.place = Place.get_from_address(address)
            instance.save()
        instance = super().update(instance, validated_data)
        return instance

    class Meta:
        model = Ride
        fields = ('driver', 'company', 'car', 'scheduled_time', 'drifts',
                  'driver_name', 'address', 'address_name', 'company_name', 'car_name')
        ref_nem = 'panel_ride_serializer'
from apps.common.filters import TimeStampDateFilter
from apps.common.views import EstandarPagination, NoPatchView
from apps.ride.models import Drift, Rate, Ride
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveUpdateDestroyAPIView,
)

from .serializers import (
    DriftSerializer,
    RateSerializer,
    RideListSerializer,
    RideSerializer,
)


class RateListAPIView(ListAPIView):
    queryset = Rate.objects.all().order_by('id')
    serializer_class = RateSerializer


class RideFilter(FilterSet):
    scheduled_begin = TimeStampDateFilter(field_name='scheduled_time', lookup_expr=('gt'))
    scheduled_end = TimeStampDateFilter(field_name='scheduled_time', lookup_expr=('lt'))

    class Meta:
        model = Ride
        fields = ['driver', 'company']


class RideListAPIView(ListAPIView):
    queryset = Ride.objects.filter().prefetch_related('driver', 'driver__car', 'arrival_place').order_by('-created')
    serializer_class = RideListSerializer
    pagination_class = EstandarPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = RideFilter


class RideCreateAPIView(CreateAPIView):
    queryset = Ride.objects.all()
    serializer_class = RideSerializer


class RideRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Ride.objects.all()
    serializer_class = RideSerializer


class DriftCreateAPIView(CreateAPIView):
    queryset = Drift.objects.all()
    serializer_class = DriftSerializer


class DriftRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Drift.objects.all().prefetch_related('passenger')
    serializer_class = DriftSerializer

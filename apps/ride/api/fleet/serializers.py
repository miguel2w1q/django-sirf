from apps.ride.models import Car
from rest_framework import serializers


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'model', 'brand', 'plate', 'capacity')

    def create(self, validated_data):
        return Car.get_car(validated_data)
    
    def update(self, instance,  validated_data):
        return Car.get_car(validated_data)


class CarBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('brand',)


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('model',)

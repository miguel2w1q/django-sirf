from django.urls import path

from .views import (
    CarBrandListAPIView,
    CarCreateListAPIView,
    CarModelListAPIView,
    CarRetrieveUpdateDestroyAPIView,
)

urlpatterns = [
    path('', CarCreateListAPIView.as_view(), name='list_create'),
    path('', CarRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
    path('<int:pk>', CarRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
    path('brand', CarBrandListAPIView.as_view(), name='list_brand'),
    path('model', CarModelListAPIView.as_view(), name='list_Model'),
]

app_name = "api_programming"

from apps.common.views import NoPatchView
from apps.ride.models import Car
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from .serializers import CarBrandSerializer, CarModelSerializer, CarSerializer


class CarCreateListAPIView(ListCreateAPIView):
    queryset = Car.objects.all().order_by('id')
    serializer_class = CarSerializer


class CarRetrieveUpdateDestroyAPIView(NoPatchView, RetrieveUpdateDestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer


class CarBrandListAPIView(ListAPIView):
    queryset = Car.objects.all()
    serializer_class = CarBrandSerializer


class CarModelListAPIView(ListAPIView):
    queryset = Car.objects.all()
    serializer_class = CarModelSerializer

from django.urls import path

from .views import (
    ArrivalUpdateAPIView,
    DriverRideRetrieveAPIView,
    EmergencyUpdateAPIView,
    HistoryListAPIView,
    PickupUpdateAPIView,
    StartRideUpdateAPIView,
)

urlpatterns = [
    path('rides', DriverRideRetrieveAPIView.as_view(), name='rides'),
    path('ride/<int:pk>/start', StartRideUpdateAPIView.as_view(), name='start_ride'),
    path('ride/<int:pk>/arrival', ArrivalUpdateAPIView.as_view(), name='arrival'),
    path('drift/<int:pk>/pickup', PickupUpdateAPIView.as_view(), name='pickup'),
    path('drift/<int:pk>/emergency', EmergencyUpdateAPIView.as_view(), name='emergency'),
    path('history', HistoryListAPIView.as_view(), name='history'),
]

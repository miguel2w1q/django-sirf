from apps.common.filters import TimeStampDateFilter
from apps.common.views import EstandarPagination, NoPatchView
from apps.ride.models import Drift, Ride
from django_filters.rest_framework import DjangoFilterBackend, FilterSet, OrderingFilter
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView

from .serializers import (
    ArrivalSerializer,
    DriverRideSerializer,
    EmergencySerializer,
    PickupSerializer,
    RideHistorySerializer,
    StartRideSerializer,
)


class DriverRideRetrieveAPIView(RetrieveAPIView):
    serializer_class = DriverRideSerializer

    def get_object(self):
        return self.request.user


class PickupUpdateAPIView(NoPatchView, UpdateAPIView):
    queryset = Drift.objects.all()
    serializer_class = PickupSerializer


class ArrivalUpdateAPIView(NoPatchView, UpdateAPIView):
    queryset = Ride.objects.all()
    serializer_class = ArrivalSerializer


class StartRideUpdateAPIView(NoPatchView, UpdateAPIView):
    queryset = Ride.objects.all()
    serializer_class = StartRideSerializer


class EmergencyUpdateAPIView(NoPatchView, UpdateAPIView):
    queryset = Drift.objects.all()
    serializer_class = EmergencySerializer


class HistoryFilter(FilterSet):
    start_arrival = TimeStampDateFilter(field_name='arrival_time', lookup_expr=('gt'))
    end_arrival = TimeStampDateFilter(field_name='arrival_time', lookup_expr=('lt'))
    order = OrderingFilter(
        fields=(
            ('arrival_time', 'arrival_time'),
            ('rate', 'rate'),
        ),
        field_labels={
            'arrival_time': 'Arrival time',
            'rate': 'Rate price',
        }
    )

    class Meta:
        model = Ride
        fields = []


class HistoryListAPIView(ListAPIView):
    serializer_class = RideHistorySerializer
    pagination_class = EstandarPagination
    filter_backends = [DjangoFilterBackend]
    filterset_class = HistoryFilter

    def get_queryset(self):
        return Ride.objects.filter(
            driver=self.request.user, finished_time__isnull=False
        ).prefetch_related('driver', 'car', 'arrival_place', 'drift_set', 'drift_set__pickup_place', 'drift_set__passenger')

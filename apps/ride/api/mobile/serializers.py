from datetime import datetime, timedelta

from apps.common.constants import (
    FINISHED,
    ON_GOING,
    PENDING,
    PICKED,
    RIDE_TYPE_RETURN,
)
from apps.common.fields import TimeStampField
from apps.ride.models import Car, Drift, Photo, Place, Ride
from apps.users.models import User
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.utils import swagger_serializer_method
from pytz import timezone
from rest_framework import serializers


class PlaceSerializer(serializers.ModelSerializer):
    distrito = serializers.SerializerMethodField()

    def get_distrito(self, place):
        return str(place.distrito.name) if place.distrito else place.secondary_distrito

    class Meta:
        model = Place
        fields = ('id', 'address', 'reference', 'latitude', 'longitude', 'distrito')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'phones')


class DriftSerializer(serializers.ModelSerializer):
    passenger = UserSerializer()
    place = PlaceSerializer(source='pickup_place')

    class Meta:
        model = Drift
        fields = ('id', 'passenger', 'place', 'status')


class RideSerializer(serializers.ModelSerializer):
    drifts = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    place = PlaceSerializer(source='arrival_place')

    def get_drifts(self, ride):
        return DriftSerializer(ride.drift_set, many=True).data

    def get_status(self, ride):
        return ride.status

    class Meta:
        model = Ride
        fields = ('id', 'type','status', 'place', 'scheduled_time', 'drifts')
        ref_name = 'mobile_ride_serializer'
        swagger_schema_fields = {
            'name': 'Ride',
            'description': 'Shows ride information',
            'type': openapi.TYPE_OBJECT,
        }


class DriverRideSerializer(serializers.ModelSerializer):
    rides = serializers.SerializerMethodField(read_only=True)

    @swagger_serializer_method(serializer_or_field=RideSerializer)
    def get_rides(self, driver):
        return RideSerializer(driver.rides_as_driver.filter(
            finished_time__isnull=True,
            scheduled_time__gte=datetime.now(timezone(settings.TIME_ZONE)) - timedelta(minutes=30)
        ), many=True).data

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'phones', 'rides')
        swagger_schema_fields = {
            'name': 'Driver and Ride information',
            'description': 'Lists information about current user\'s rides',
        }


class PhotoSerializer(serializers.ModelSerializer):
    created_time = TimeStampField(required=True)

    class Meta:
        model = Photo
        fields = ('created_time', 'base64', 'latitude', 'longitude')


class StartRideSerializer(serializers.ModelSerializer):
    time = TimeStampField(source='started_time', required=True)
    latitude = serializers.DecimalField(source='started_latitude', max_digits=9, decimal_places=6, required=True)
    longitude = serializers.DecimalField(source='started_longitude', max_digits=9, decimal_places=6, required=True)
    plate = serializers.CharField(write_only=True)

    class Meta:
        model = Ride
        fields = ('time', 'latitude', 'longitude', 'plate')
        swagger_schema_fields = {
            'time': 'Start Ride Endpoint',
            'description': 'Updates Rides\'s location and started time',
            'example': {
                'time': 1592700943,
                'latitude': '-12.000001',
                'longitude': '-77',
                'plate': 'ASD77'
            }
        }

    def update(self, instance, validated_data):
        car, _ = Car.objects.get_or_create(plate=validated_data.pop('plate'))
        super().update(instance, validated_data)
        if(instance.type == RIDE_TYPE_RETURN):
            instance.arrival_time = validated_data.get('started_time')
            instance.arrival_latitude = validated_data.get('started_latitude')
            instance.arrival_longitude = validated_data.get('started_longitude')
        instance.car = car
        instance.save()
        return instance


class ArrivalSerializer(serializers.ModelSerializer):
    time = TimeStampField(source='arrival_time', required=True)
    photos = PhotoSerializer(source='arrival_photos', many=True, required=False)
    latitude = serializers.DecimalField(source='arrival_latitude', max_digits=9, decimal_places=6)
    longitude = serializers.DecimalField(source='arrival_longitude', max_digits=9, decimal_places=6)

    class Meta:
        model = Ride
        fields = ('time', 'latitude', 'longitude', 'photos')

    def update(self, instance, validated_data):
        instance.finished_time = validated_data.get('arrival_time')
        instance.finished_latitude = validated_data.get('arrival_latitude')
        instance.finished_longitude = validated_data.get('arrival_longitude')
        if(instance.type == RIDE_TYPE_RETURN):
            instance.save()
            return instance
        photos_data = validated_data.pop('arrival_photos', {})
        super().update(instance, validated_data)
        instance.update_coords()
        for photo_data in photos_data:
            photo_data = dict(photo_data)
            photo_data.update({
                'created_time': photo_data.get('created_time').replace(tzinfo=timezone('UTC'))
            })
            instance.arrival_photos.add(Photo.objects.create(**photo_data))
        instance.save()
        return instance


class PickupSerializer(serializers.ModelSerializer):
    time = TimeStampField(source='pickup_time', required=True)
    photos = PhotoSerializer(source='pickup_photos', many=True, required=False)
    latitude = serializers.DecimalField(source='pickup_latitude', max_digits=9, decimal_places=6)
    longitude = serializers.DecimalField(source='pickup_longitude', max_digits=9, decimal_places=6)

    class Meta:
        model = Drift
        fields = ('time', 'latitude', 'longitude', 'photos')

    def update(self, instance, validated_data):
        photos_data = validated_data.pop('pickup_photos', {})
        instance.status = PICKED
        super().update(instance, validated_data)
        instance.update_coords()
        for photo_data in photos_data:
            photo_data = dict(photo_data)
            photo_data.update({
                'created_time': photo_data.get('created_time').replace(tzinfo=timezone('UTC'))
            })
            instance.pickup_photos.add(Photo.objects.create(**photo_data))
        instance.save()
        instance.ride.compute_rate()
        return instance


class EmergencySerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(source='pickup_photos', many=True, required=False)

    class Meta:
        model = Drift
        fields = ('status', 'status_description', 'photos')

    def update(self, instance, validated_data):
        photos_data = validated_data.pop('pickup_photos', {})
        super().update(instance, validated_data)
        for photo_data in photos_data:
            photo_data = dict(photo_data)
            photo_data.update({
                'created_time': photo_data.get('created_time').replace(tzinfo=timezone('UTC'))
            })
            instance.pickup_photos.add(Photo.objects.create(**photo_data))
        instance.save()
        instance.ride.compute_rate()
        return instance


class DriftHistorySerializer(serializers.ModelSerializer):
    passenger = UserSerializer()
    place = PlaceSerializer(source='pickup_place')

    class Meta:
        model = Drift
        fields = ('id', 'passenger', 'pickup_time', 'place', 'rate', 'rate_code')


class RideHistorySerializer(serializers.ModelSerializer):
    drifts = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    total = serializers.DecimalField(source='rate', max_digits=9, decimal_places=6, read_only=True)

    def get_drifts(self, ride):
        return DriftHistorySerializer(ride.drift_set, many=True).data

    def get_status(self, ride):
        if ride.started_time:
            return ON_GOING if ride.started_time < ride.arrival_time else FINISHED
        else:
            return PENDING

    class Meta:
        model = Ride
        fields = ('id', 'scheduled_time', 'arrival_time', 'drifts', 'total', 'status')
        swagger_schema_fields = {
            'name': 'Ride',
            'description': 'Shows ride information',
            'type': openapi.TYPE_OBJECT,
        }

from django.urls import path

from .views import (
    DriverProgrammingDataAPIView,
    ProgrammingCleanAPIView,
    ProgrammingDestroyAPIView,
    ProgrammingFinishAPIView,
    ProgrammingUpdateAPIView,
    ProgrammingUploadAPIView,
    RideCreateAPIView,
    ShiftAPIView,
)

urlpatterns = [
    path('file', ProgrammingUploadAPIView.as_view(), name='file'),
    path('shift/<int:id>', ShiftAPIView.as_view(), name='shift'),
    path('data/', DriverProgrammingDataAPIView.as_view(), name='driver_drift'),
    path('data/<int:id>', DriverProgrammingDataAPIView.as_view(), name='driver_drift'),
    path('ride', RideCreateAPIView.as_view(), name='ride'),
    path('remove/<int:pk>', ProgrammingDestroyAPIView.as_view(), name='programming_remove'),
    path('update/<int:pk>', ProgrammingUpdateAPIView.as_view(), name='programming_update'),
    path('clean/<int:id>', ProgrammingCleanAPIView.as_view(), name='programming_clean'),
    path('finish/<int:id>', ProgrammingFinishAPIView.as_view(), name='programming_finish'),
]

app_name = "api_programming"

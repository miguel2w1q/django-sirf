from apps.ride.models import Drift, Place, Programming, Rate, Ride
from apps.users.models import User
from rest_framework import serializers


class PlaceSerializer(serializers.ModelSerializer):
    distrito = serializers.SerializerMethodField()
    rate = serializers.SerializerMethodField()

    def get_distrito(self, place):
        return str(place.distrito) if place.distrito else place.secondary_distrito.title()

    def get_rate(self, place):
        return place.rate.code if place.rate else ''
    
    class Meta:
        ref_name = 'place_programming_panel'
        model = Place
        fields = ('id', 'address', 'reference', 'latitude', 'longitude', 'distrito', 'rate')


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    def get_first_name(self, user):
        return user.first_name.title()

    def get_last_name(self, user):
        return user.last_name.title()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'phones')


class DriverSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    capacity = serializers.SerializerMethodField()

    def get_capacity(self, user):
        return user.car.capacity if user.car else 0

    def get_first_name(self, user):
        return user.first_name.title()

    def get_last_name(self, user):
        return user.last_name.title()

    class Meta:
        ref_name = 'driver_serializer_panel'
        model = User
        fields = ('username', 'first_name', 'last_name', 'capacity')


class ShiftSerializer(serializers.ModelSerializer):
    class Meta:
        model = Programming
        fields = ('id', 'scheduled_time',)


class DataDriftSerializer(serializers.ModelSerializer):
    passenger = UserSerializer()
    place = PlaceSerializer()

    class Meta:
        model = Programming
        fields = ('id', 'ride_type', 'passenger', 'place')


class ProgrammingField(serializers.RelatedField):
    def to_internal_value(self, data):
        return Programming.objects.filter(id__in=data.split(','))

    def to_representation(self, value):
        return value


class DriverField(serializers.RelatedField):
    def to_internal_value(self, data):
        return User.objects.get(username=data)

    def to_representation(self, value):
        return value.username


class RideCreateSerializer(serializers.ModelSerializer):
    programming = ProgrammingField(queryset=Programming, many=True, write_only=True)
    driver = DriverField(queryset=User, write_only=True, required=True)
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Ride
        fields = ('id', 'driver', 'programming')

    def create(self, validated_data):
        driver = validated_data.pop('driver')
        programming_data = validated_data.pop('programming')[0]
        info = programming_data[0]
        programming_number = info.company.programming_number
        ride_data = {
            'driver': driver,
            'car': driver.car,
            'company': info.company,
            'type': info.ride_type,
            'scheduled_time': info.scheduled_time,
            'arrival_place': info.company.address,
            'programming_number': programming_number
        }

        if programming_data:
            ride = Ride.objects.create(**ride_data)
            ride.slug = f'RC-{ride.id}'
            ride.save()
        else:
            return None
        for data in programming_data:
            drift_data = {
                'ride': ride,
                'pickup_place': data.place,
                'passenger': data.passenger,
                'programming_number': programming_number
            }
            data.drift = Drift.objects.create(**drift_data)
            data.drift.get_rate()
            data.drift.save()
            data.save()
        return ride


class ProgrammingUpdateSerializer(serializers.ModelSerializer):
    address = serializers.CharField(write_only=True)
    reference = serializers.CharField(write_only=True)
    distrito = serializers.CharField(write_only=True)
    rate = serializers.CharField(write_only=True)

    class Meta:
        model = Programming
        fields = ('address', 'reference', 'distrito', 'rate')

    def update(self, instance, validated_data):
        rate, _ = Rate.objects.get_or_create(code=validated_data.pop('rate'))
        place, _ = Place.objects.get_or_create(address=validated_data.pop('address').upper())
        setattr(place, 'rate', rate)
        setattr(place, 'reference', validated_data.get('reference', ''))
        setattr(place, 'secondary_ditrito', validated_data.get('distrito', ''))
        place.update_coords()
        place.save()
        instance.place = place
        instance.save()
        return instance

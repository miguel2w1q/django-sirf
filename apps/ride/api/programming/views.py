import django_rq
from apps.ride.models import Company, Programming, Ride
from apps.users.models import User
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import (
    DataDriftSerializer,
    DriverSerializer,
    ProgrammingUpdateSerializer,
    RideCreateSerializer,
    ShiftSerializer,
)
from .utils import finish_programming, generate_programing


class ShiftAPIView(APIView):
    def get(self, request, **kwargs):
        number = Programming.objects.filter(company_id=self.kwargs.get('id'), drift__isnull=False).distinct('drift__ride').count()
        qs_shifts = Programming.objects.filter(company=self.kwargs.get(
            'id'), drift__isnull=True).distinct('scheduled_time').order_by('scheduled_time')
        return Response({
            'programmed': number,
            'shifts': ShiftSerializer(qs_shifts, many=True).data
        }, status=200)


class DriverProgrammingDataAPIView(APIView):
    def get(self, request, **kwargs):
        programming = Programming.objects.get(id=self.kwargs.get('id'))
        company = programming.company
        scheduled_time = programming.scheduled_time
        qs_driver = User.objects.filter(groups__name='driver').select_related('car').exclude(
            id__in=User.objects.filter(
                rides_as_driver__scheduled_time=scheduled_time, groups__name='driver').values_list('id', flat=True))
        qs_drifts = Programming.objects.filter(
            drift__isnull=True,
            company_id=company.id,
            scheduled_time=scheduled_time
        ).order_by('ride_type', 'place__rate__code', 'place__distrito'
                   ).prefetch_related('place', 'place__distrito', 'place__rate', 'passenger')
        return Response({
            'drivers': DriverSerializer(qs_driver, many=True).data,
            'drifts': DataDriftSerializer(qs_drifts, many=True).data,
        }, status=200)


class RideCreateAPIView(CreateAPIView):
    queryset = Ride.objects.all()
    serializer_class = RideCreateSerializer


class ProgrammingUploadAPIView(APIView):
    parser_classes = [MultiPartParser]

    def put(self, request):
        excel_file = request.data['file']
        company = Company.objects.get(id=request.data['company_id'])
        django_rq.enqueue(generate_programing, excel_file, company)
        return Response({'message': 'processing'}, status=204)


class ProgrammingDestroyAPIView(DestroyAPIView):
    queryset = Programming.objects.all()


class ProgrammingUpdateAPIView(UpdateAPIView):
    queryset = Programming.objects.all()
    serializer_class = ProgrammingUpdateSerializer


class ProgrammingCleanAPIView(APIView):
    def get(self, request, **kwargs):
        Programming.objects.filter(company_id=self.kwargs.get('id'), drift__isnull=True).delete()
        return Response('Se limpió la programación', status=200)


class ProgrammingFinishAPIView(APIView):
    def get(self, request, **kwargs):
        django_rq.enqueue(finish_programming, self.kwargs.get('id'))
        return Response('Se limpió la programación', status=200)

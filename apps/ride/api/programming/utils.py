import datetime
import traceback

from apps.common.constants import RIDE_TYPE_GOING, RIDE_CREATION_TYPE_MANUAL
from apps.ride.models import Company, Programming, Ride, Drift
from django.utils.timezone import localtime
from openpyxl import load_workbook

from apps.common.utils import ExcelFile

header_color = "900C3F"
row_color_1 = "DAF7A6"
row_color_2 = "FFF9D8"
white_color = "FFFFFF"
black_color = "000000"


def generate_programing(excel_file, company):
    colnames = ['SERVICIO', 'HORA', 'DNI', 'TRABAJADOR', 'DIRECCION', 'DISTRITO', 'COB', 'REFERENCIA', 'TELEF']
    try:
        wb = load_workbook(excel_file)
        sheet = wb.active
        row = 0
        arrival_date = sheet.cell(row=1, column=3).value
        col_indices = {cell.value: n for n, cell in enumerate(tuple(sheet.rows)[1]) if cell.value in colnames}
        col_indices['SERVICIO'] = 1
        if len(col_indices) != len(colnames):
            return
        for row in tuple(sheet.rows)[2:]:
            if row[col_indices['DNI']].value is None:
                break
            data = {
                'company': company,
                'dni': str(row[col_indices['DNI']].value),
                'full_name': str(row[col_indices['TRABAJADOR']].value),
                'phones': str(row[col_indices['TELEF']].value),
                'arrival_date': arrival_date,
                'arrival_time': row[col_indices['HORA']].value,
                'service': str(row[col_indices['SERVICIO']].value),
                'address': str(row[col_indices['DIRECCION']].value).upper(),
                'distrito': str(row[col_indices['DISTRITO']].value).lower(),
                'reference': str(row[col_indices['REFERENCIA']].value),
                'rate_code': str(row[col_indices['COB']].value).strip()
            }
            data = Programming.obtain_data_from_excel(data)
            try:
                if data:
                    Programming.objects.create(**data)
            except Exception as ex:
                traceback.print_exc(ex)
    except Exception as ex:
        traceback.print_exc(ex)




def finish_programming(company_id):
    company = Company.objects.get(id=company_id)
    Programming.objects.filter(company=company, drift__isnull=True).delete()
    rides = Ride.objects.filter(
        company=company,
        programming_number=company.programming_number).order_by(
            'scheduled_time').prefetch_related(
        'driver', 'car', 'arrival_place', 'drift_set', 'drift_set__pickup_place', 'drift_set__passenger')
    company.programming_number += 1
    company.save()
    ef = ExcelFile()
    ef.add_email(company.email)
    data = ["SERVICIOS", "HORA", "CONDUCTOR", "TRABAJADOR", "DIRECCION", "REFERENCIA", "TELEFONO", "COB", "TAR"]
    ef.add_columns_width(1, [12, 8, 27, 45, 45, 45, 27, 12, 12])
    ef.row(2, 2, data, header_color, white_color)
    row = 3
    row_color_counter = 0
    for ride in rides:
        st = localtime(ride.scheduled_time).time()
        ride_type = 'INGRESO' if ride.type == RIDE_TYPE_GOING else 'SALIDA'
        driver = ride.driver.full_name
        row_color = row_color_1 if row_color_counter % 2 == 0 else row_color_2
        row_color_counter += 1
        for drift in ride.drift_set.all():
            place = drift.pickup_place
            data = [
                ride_type,
                st.strftime("%H:%M"),
                driver,
                drift.passenger.full_name,
                place.address,
                place.reference,
                ' - '.join(drift.passenger.phones),
                place.rate.code,
                f"S/ {place.rate.price}"
            ]
            ef.row(2, row, data, row_color, black_color)
            row += 1
    Programming.objects.filter(company=company).delete()
    ef.send()

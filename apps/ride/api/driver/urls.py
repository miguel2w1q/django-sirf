
from django.urls import path

from .views import DriverCreateListAPIView, DriverRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', DriverCreateListAPIView.as_view(), name='list_create'),
    path('', DriverRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
    path('<int:pk>', DriverRetrieveUpdateDestroyAPIView.as_view(), name='retrieve_update_destroy'),
]

app_name = "api_driver"

from apps.common.views import NoPatchView
from django.contrib.auth.models import Group
from apps.users.models import User
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from .serializers import DriverSerializer


class DriverCreateListAPIView(ListCreateAPIView):
    queryset = User.objects.filter(groups__name='driver').select_related('car').order_by('id')
    serializer_class = DriverSerializer


class DriverRetrieveUpdateDestroyAPIView(NoPatchView, RetrieveUpdateDestroyAPIView):
    queryset = User.objects.filter(groups__name='driver').select_related('car')
    serializer_class = DriverSerializer

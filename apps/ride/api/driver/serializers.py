from apps.ride.api.fleet.serializers import CarSerializer
from apps.ride.models import Car
from apps.users.models import User
from django.contrib.auth.models import Group
from rest_framework import serializers

def driver_group(): 
    return Group.objects.get(name='driver')

class DriverSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=6, max_length=6, allow_blank=True, write_only=True)
    car = CarSerializer()

    def get_vehicle_name(self, driver):
        return str(driver.car)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email',
                  'phones', 'email', 'password', 'car', 'plain_password')

    def create(self, validated_data):
        password = validated_data.pop('password')
        car = Car.get_car(validated_data.pop('car'))
        user = User.objects.create(**validated_data)
        user.car = car
        user.groups.add(driver_group())
        user.update_password(password)
        return user

    def update(self, instance, validated_data):
        password = validated_data.pop('password', '')
        car = Car.get_car(validated_data.pop('car'))
        instance.car = car
        instance = super().update(instance, validated_data)
        if(password):
            instance.update_password(password)
        else:
            instance.save()
        return instance

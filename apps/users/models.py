from django.contrib.auth.models import AbstractUser, Group
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.urls import reverse

def passenger_group():
    return Group.objects.get(name='passenger')


class User(AbstractUser):
    phones = ArrayField(models.CharField(max_length=15), null=True, blank=True)
    car = models.ForeignKey('ride.Car', on_delete=models.SET_NULL, null=True, blank=True)
    plain_password = models.CharField('visible password', max_length=16, blank=True, null=True)

    def update_password(self, password):
        self.plain_password = password
        self.set_password(password)
        self.save()

    @property
    def full_name(self):
        return f"{self.last_name}, {self.first_name}"

    @classmethod
    def obtain_from_excel(cls, data):
        dni = data.get('dni').strip().replace(' ', '')
        if not dni:
            return None
        phones = []
        if '-' in data.get('phones'):
            phones = [phone.strip() for phone in data.get('phones').split('-')]
        elif '/' in data.get('phones'):
            phones = [phone.strip() for phone in data.get('phones').split('-')]
        else:
            phones.append(data.get('phones').strip())
        try:
            user = cls.objects.get(username=dni)
            user.phones = phones
        except cls.DoesNotExist:
            full_name = [n for n in data.get('full_name').strip().split(' ') if n != '']
            first_name = ' '.join(full_name[:2])
            last_name = ' '.join(full_name[2:])
            user = User.objects.create(username=dni, first_name=first_name, last_name=last_name, phones=phones)
            user.groups.add(passenger_group())
            user.set_password(dni)
        user.save()
        return user

    @classmethod
    def obtain_from_drift(cls, data):
        dni = data.get('dni').strip().replace(' ', '')
        user, created = cls.objects.get_or_create(username=dni)
        if created:
            user.first_name = data.get('first_name')
            user.last_name = data.get('last_name')
            user.groups.add(passenger_group())
            user.save()
        return user

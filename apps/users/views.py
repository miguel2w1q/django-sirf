from apps.ride.models import Car, Company
from django.contrib.auth import get_user_model, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import RedirectView

User = get_user_model()


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        if self.request.user.groups.filter(name='operator').exists():
            return reverse("panel:programming")
        logout(self.request)
        return reverse("account_login")


user_redirect_view = UserRedirectView.as_view()
